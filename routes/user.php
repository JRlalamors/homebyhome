<?php

Route::group(['namespace' => 'User'], function(){

	/*
	* Agent Routes
	**/
	require base_path('routes/agent.php');

	/*
	* Customer Routes
	**/
	require base_path('routes/customer.php');
});
