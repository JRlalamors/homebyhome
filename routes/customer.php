<?php 

Route::group(['prefix' => 'customer', 'as' => 'customer.'], function() {
	Route::get('account', 'CustomerController@account')->name('account');
	Route::get('agent/{agent}/overview', 'CustomerController@agentOverview')->name('agent-overview');
	Route::get('list/{list}/agent/{agent}/proposal', 'CustomerController@agentProposal')->name('list.agent.proposal');
	Route::get('list/{list}/agent/{agent}/hire', 'CustomerController@hireAgent')->name('list.agent.hire');

	// Profile
	Route::get('profile', 'CustomerController@profile')->name('profile');
	Route::post('profile/update', 'CustomerController@updateProfile')->name('profile.update');

	// Add list
	Route::get('list/add', 'CustomerController@addList')->name('list.add');
	Route::post('list/validate-form', 'CustomerController@validateForm');
	Route::post('list/store', 'CustomerController@storeList');

	// ajax
    Route::post('list/{list}/update', 'CustomerController@updateList')->name('list-update');
	Route::post('agent/{agent}/send-message', 'CustomerController@sendMessage');

	// chat
	Route::post('set-chat-id', 'CustomerController@setChatID');
	Route::post('get-agent-chatid', 'CustomerController@getAgentChatID');

});
