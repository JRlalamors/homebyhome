@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop


@section('page-title')
	Find an Agent
@stop

<!-- Content -->
@section('content')
<section id="agents-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Agency</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">View Full Profile</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse( $agents as $agent )
                                <tr>
                                    <td class="text-center">{{ $agent->asUser->name }}</td>
                                    <td class="text-center">{{ $agent->asUser->email }}</td>
                                    <td class="text-center">HOME BY HOME</td>
                                    <td class="text-center"><a href="tel:@phoneNum( $agent->asUser->phone_num )" class="default">{{ $agent->asUser->phone_format }}</a></td>
                                    <td class="text-center"><a href="{{ route('admin.agents.profile', $agent) }}" class="blue">View Profile</a></td>
                                </tr>
                            @empty
                                <tr><td colspan="5">Agents not found..</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                    @if( $agents instanceof \Illuminate\Pagination\LengthAwarePaginator )
                        {{ $agents->links() }}
                    @endif
                </div>

                <a href="{{ route('register') }}" class="btn btn-primary">Register Now</a>

			</div>
		</div>
	</div>
</section>

@stop

<!-- Added Scripts -->
@section('scripts')

@stop