@extends('layouts.default')

@section('styles')
<style type="text/css">
	
	.experience-areas {
		margin-bottom: 40px;
		border-bottom: 2px solid #1FC0DA;
		padding-bottom: 30px;
	}

	.areas-serviced {
		margin: 30px 0 0;
	}

	.areas-serviced li {
		margin: 0 0 10px;
		color: #727272;
		padding-left: 30px;
	}

	body.agent-proposal #proposal-profile .profile .promo .dc.fixed {
		font-size: 45px;
		margin: 25px 0;
	}

</style>
@stop

@section('body-class')
	agent-proposal site-notice
@stop
@section('page-title')
	{{ $agent->asUser->name }}'s Proposal for {{ $customer }}
@stop

@section('content')
<section id="proposal-profile">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="profile">
					<div class="item">
						<div class="photo">
							@if( $agent->asUser->profile_photo )
								<img class="img-responsive" src="{{ $agent->asUser->profile_photo }}">
							@else
								<img class="img-responsive" src="{{ isset( $zillowApi['proInfo']['photo'] ) ? $zillowApi['proInfo']['photo'] : asset('images/user-blank.svg') }}">
							@endif
						</div>
					</div><div class="item">
						<h2 class="name">{{ $agent->asUser->name }}</h2>
						<div class="company">
							{{ $zillowApi != null ? $zillowApi['proInfo']['businessName'] : '' }} <br>

							{{ $agent->asUser->email }} <br>

							{{ isset( $zillowApi['proInfo']['phone'] ) ? $zillowApi['proInfo']['phone'] : $agent->phone_num }}
						</div>

						<div class="company">
							<ul class="list-inline">
								@if($agent->zillow_url)
									<li>
										<a href="{{ $agent->zillow_username }}" target="_blank">
											<img src="{{ asset('images/zillow-icon.png') }}">
										</a>
									</li>
								@endif
								@if($agent->linkedin_url)
									<li>
										<a href="{{ $agent->linkedin_url }}" target="_blank">
											<img src="{{ asset('images/linkedin-icon.png') }}">
										</a>
									</li>
								@endif
								@if($agent->website_url)
									<li>
										<a href="{{ $agent->website_url }}" target="_blank">
											<img src="{{ asset('images/earth-icon.png') }}">
										</a>
									</li>
								@endif
								@if($agent->getOtherData('facebook_url'))
									<li>
										<a href="{{ $agent->getOtherData('facebook_url') }}" target="_blank">
											<img src="{{ asset('images/facebook-icon.png') }}">
										</a>
									</li>
								@endif
								@if($agent->getOtherData('yelp_url'))
									<li>
										<a href="{{ $agent->getOtherData('yelp_url') }}" target="_blank">
											<img src="{{ asset('images/yelp-icon.png') }}">
										</a>
									</li>
								@endif
							</ul>
						</div>
						
						<div class="rating">
							@php
								$avgRating = $zillowApi['proInfo']['avgRating'] ? intval($zillowApi['proInfo']['avgRating']) : 0;	
								$remaining = 5 - $avgRating;

								for( $i = 1; $i <= $avgRating; $i++ ) 
								{
									echo '<i class="fa fa-star" aria-hidden="true"></i> ';
								}

								if( fmod($avgRating, 1) !== 0.00 ) 
								{
									echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
								}

								for( $i = 1; $i <= $remaining; $i++ ) 
								{
									echo '<i class="fa fa-star-o" aria-hidden="true"></i> ';
								}
							@endphp
						</div>
						<div class="ctrls">
							<a href="javascript:;" class="btn btn-block btn-blue  " data-toggle="modal" data-target="#contactMe">Contact Me</a>

							@if( !$list->hasSelectedAProposal() )
								<a href="{{ route('customer.list.agent.hire', [$list, $agent]) }}" class="btn btn-block btn-pink   confirm" data-confirmtitle="Hire Agent?" data-confirmtext="Are you sure you want to hire this agent?">Hire Me</a>
							@endif
						</div>
					</div><div class="item">
						<div class="promo">
							<div class="dc {{ $proposal->rebate_type == 'fixed' ? 'fixed' : '' }}">
								{{ $proposal->rebate_format }}
							</div>
							@if($list->isBuyer())
								<div class="dc-text">Agent commission rebate.</div>
								@if($proposal->rebate_type == 'fixed')
									<div class="dc-text">
										Save {{ $proposal->approximate }} when you use {{ $agent->asUser->first_name }} to buy a home.
									</div>
								@else
									<div class="dc-text">
										Save approximately <br>{{ $proposal->approximate }} when you use {{ $agent->asUser->first_name }} to buy a home.
									</div>
								@endif
							@else
								<div class="dc-text">Listing commission rate.</div>
								<div class="dc-text">
									Save approximately <br>{{ $proposal->approximate }} when you use {{ $agent->asUser->first_name }} to sell your home.
								</div>
							@endif

							{{--<div class="dc-text">
								Save approximately <br> {{ $proposal->approximate }}
							</div>--}}
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="proposal-info" class="default-section">

	<div class="container experience-areas">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h2 class="heading">Experience Levels</h2>
				<div class="textarea list-satisfaction">
				 	<div class="listitem">
				 		<div class="title">Local Knowledge</div><div class="content">{{ $agent->ratingPercentage('localknowledgeRating') }} Satisfaction</div>
				 	</div>
				 	<div class="listitem">
				 		<div class="title">Process Expertise</div><div class="content">{{ $agent->ratingPercentage('processexpertiseRating') }} Satisfaction</div>
				 	</div>
				 	<div class="listitem">
				 		<div class="title">Negotiations</div><div class="content">{{ $agent->ratingPercentage('negotiationskillsRating') }} Satisfaction</div>
				 	</div>
				 	<div class="listitem">	
				 		<div class="title">Responsiveness</div><div class="content">{{ $agent->ratingPercentage('responsivenessRating') }} Satisfaction</div>
				 	</div>
			 	</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<h2 class="heading">Areas Serviced</h2>
				@if( isset($agent->zillowApi()['proInfo']['serviceAreas']['area']) )
				
					<ul class="list-unstyled areas-serviced" style="display: inline-block">
						@foreach( $agent->zillowApi()['proInfo']['serviceAreas']['area'] as $area )
							@if($loop->iteration % 5 == 0)
								</ul><ul class="list-unstyled areas-serviced" style="display: inline-block">
							@endif
							<li><strong>{{ $area }}</strong></li>
						@endforeach
					</ul>
				@endif
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-xs-12">
			 	<h2 class="heading">Why Should You Hire Me?</h2>
			 	<div class="textarea">
		 			{{ $proposal->getOtherData('propose_1') ? $proposal->getOtherData('propose_1') : $agent->getOtherData('question_1') }} 
			 	</div>

			 	<h2 class="heading">What Do I Do Better Than Most Agents?</h2>
			 	<div class="textarea">
		 			{{ $proposal->getOtherData('propose_2') ? $proposal->getOtherData('propose_2') : $agent->getOtherData('question_2') }} 
			 	</div>

			 	<h2 class="heading">What Do Others Say About Me?</h2>			 	
			 	@if( isset($zillowApi['proReviews']['review']) && count($zillowApi['proReviews']['review']) )
					<div class="cs-list">
						<ul>
							@foreach( $zillowApi['proReviews']['review'] as $review )
								<li>
									<div class="cs-item">
										<div class="top">
											<div class="stars">
												@php
													$r1 = floatval( isset($review['localknowledgeRating']) ? $review['localknowledgeRating'] : 0 );
													$r2 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
													$r3 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
													$r4 = floatval( isset($review['negotiationskillsRating']) ? $review['negotiationskillsRating'] : 0 );

													$raRating = ($r1 + $r2 + $r3 + $r4) / 4;

													$remaining = 5 - $raRating;

													for( $i = 1; $i <= $raRating; $i++ ) {
														echo '<i class="fa fa-star" aria-hidden="true"></i> ';
													}

													if( fmod($raRating, 1) !== 0.00 ) {
														echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
													}

													for( $i = 1; $i <= $remaining; $i++ ) 
													{
														echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
													}
												@endphp
											</div>
											<div class="date">{{ isset($review['reviewDate']) ? $review['reviewDate'] : '' }}</div>
										</div>
										<div class="cs-name">{{ ucwords(isset($review['reviewer']) ? $review['reviewer'] : '') }}</div>

										<div class="cs-info">{{ isset($review['reviewSummary']) ? $review['reviewSummary'] : '' }}</div>
										<div class="cs-message">{!! isset($review['description']) ? $review['description'] : '' !!}</div>
									</div>
								</li>		
							@endforeach						
						</ul>
					</div>
				@endif
			 </div>
		</div>	
	</div>
</section>
@stop

@section('afteElements')
	<div id="contactMe" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Send Me a Message</h4>
				</div>
				<div class="modal-body">
					<div class="default-form">
						{{--<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" class="form-control">
						</div>--}}

						<div class="form-group">
							<label>Message</label>
							<textarea rows="10" name="message" class="form-control"></textarea>
						</div>

						<div class="form-group text-right">
							<button class="btn btn-success send-message" data-agentid="{{ $agent->id }}">Send</button>
							<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@stop

@section('scripts')
<script type="text/javascript">
	// verify csrf token
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('.send-message').click(function(){

		$agentID = $(this).data('agentid');

		$data = {
			//'subject' : $('#contactMe input[name="title"]').val(),
			'message' : $('#contactMe textarea[name="message"]').val()
		}

		$.ajax({
			type: 'POST',
			url: baseUrl + '/customer/agent/' + $agentID + '/send-message',
			data: $data,
			success: function($response) {
				if( $response == 'success' ) 
				{
					swal('Success', 'Message Sent.', 'success');
					$('#contactMe').modal('hide');
				} else {
					swal('Error', 'Something went wrong', 'error');
				}
			}
		});

	});
</script>
@stop