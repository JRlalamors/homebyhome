@extends('layouts.default')

@section('styles')
	<style type="text/css">
		
	</style>
@stop

@section('body-class')
	customer-profile pagetitle-off
@stop

@section('content')
<section id="customer-profile" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-4">
				<div class="photo"><img class="img-responsive" src="{{ $customer->asUser->profile_photo ? $customer->asUser->profile_photo : asset('images/user-blank.svg') }}"></div>
			</div>
			<div class="col-xs-12 col-md-8">
				<div class="info">
					<h1 class="name">{{ $customer->asUser->name }}</h1>
					{{--<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> Highlands Ranch, CO</div>--}}
					@if( $customer->asUser->city )
						<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $customer->asUser->city . " " . $customer->asUser->state }}</div>
					@endif
					<div class="moreinfo">
						<div class="listitem">
							<div class="title">Email</div><div class="content"><a href="mailto:{{ $customer->asUser->email }}">{{ $customer->asUser->email }}</a></div>
						</div>
						<div class="listitem">
							<div class="title">City</div><div class="content">{{ $customer->asUser->city ?: '--' }}</div>
						</div>
						<div class="listitem">
							<div class="title">State</div><div class="content">{{ $customer->asUser->state ?: '--' }}</div>
						</div>
						<div class="listitem">
							<div class="title">Phone</div><div class="content"><a href="tel:@phoneNum($authUser->phone_num)">{{ $customer->asUser->phone_format }}</a></div>
						</div>
					</div>
					<div class="ctrls">
						@php $deal = $list; @endphp
						<a href="{{ route('agent.list.details', $deal) }}" class="btn btn-pink btn-round hvr-rectangle-out">Edit Proposal</a>
						<a href="{{ route('agent.customer.profile', $customer) }}" class="btn btn-blue btn-round hvr-rectangle-out">See Customer's View</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="customer-moreinfo" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-push-4">

				<div class="cp-container">
					<h2 class="heading">About My Home Needs</h2>
					<div class="content">
						@if($list->list_type == 'buyer')
							<p>Hi, my name is {{ $customer->asUser->name }}.  I am looking to buy a {{ $list->property->human_display }} in the {{ $list->complete_address }} area {{ $list->text_until ? $list->text_until : $list->until }}.</p>
							<p>My budget is approximately {{ $list->text_budget ? $list->text_budget : $list->budget }}. and {{ $customer->lender_status }} for a mortgage.
								@if($list->bedrooms && $list->bathrooms)<br><br>I am looking for a home that is {{ $list->bedrooms }} bedrooms and {{ $list->bathrooms }} bathrooms.@endif
							</p>
						@else
							<p>Hi, my name is {{ $customer->asUser->name }}. I am looking to sell my {{ number_format($list->sq_feet_max, 0) }} square foot {{ $list->property->human_display }} that is {{ $list->bedrooms }} bedrooms and {{ $list->bathrooms }} in the {{ $list->complete_address }} area. {{ $list->text_until ? $list->text_until : $list->until }}.
								<br><br>I believe my home is worth between {{ $list->text_budget ? $list->text_budget : $list->budget }}.
								<br><br>I look forward to hearing from you.
							</p>
						@endif
						{{--<p>Hi, my name is Scott Sterling.  I am looking to buy a townhouse in the Highlands Ranch, CO area in the next month.</p>
						<p>My budget is approximately $400,000 and I have not been pre-approved and I have not been pre-qualified for a mortgage. I am looking for a home that is 3 bedrooms and 2 bathrooms.</p>--}}
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-8">
				<div class="sidebar cp-container">
					<div class="listitem">
						<div class="title">Date Signed Up</div>
						<div class="content">@dateFormat($list->created_at)</div>
					</div>
					<div class="listitem">
						<div class="title">Proposal Submitted</div>
						@php
							$latestProposal = $list->proposals->sortByDesc('created_at')->first();
						@endphp
						@if ($latestProposal)
							<div class="content">{{ $latestProposal->created_at->format('F d, Y') }}</div>
						@endif
					</div>
					<div class="listitem">
						<div class="title">Have They Selected An Agent</div>
						<div class="content">{{ $list->selectedAgent ? 'Yes' : 'Not Yet' }}</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@stop

@section('scripts')
@stop