@if( Session::get('success') )
	<div class="alert alert-success alert-close">
		<a href="#" title="Close" class="glyph-icon alert-close-btn icon-remove"></a>
        <div class="bg-success alert-icon">
            <i class="glyph-icon icon-success"></i>
        </div>
        <div class="alert-content">
        	{{ Session::get('success') }}
        </div>	
	</div>
@endif