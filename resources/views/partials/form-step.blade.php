<div class="panel">
	<div class="panel-body">
			
		@include('partials.form-ajax-errors')

		@if( $form == 'customer-registration' )

			@include('partials.inc.customer-registration')
			@include('layouts.partials.form-loader')

		@endif

		<!-- AGENT REGISTRATION FORM -->
		@if($form == 'agent-registration')

			@include('partials.inc.agent-registration')
			@include('layouts.partials.form-loader')

		@endif

		<!-- CUSTOMER ADD LIST FORM -->
		@if( $form == 'customer-addlist' )

			@include('partials.inc.customer-addlist')
			@include('layouts.partials.form-loader')

		@endif		

	</div>
</div>

@section('scripts')
<script>
     function initMap() {

     	if( $('#map').length )
     	{
     		// return;
		    var map = new google.maps.Map(document.getElementById('map'));

		    var card = document.getElementById('pac-card');
            var input = document.getElementById('postal_code');

		    var options = {
		      types: ['(regions)'],
		      componentRestrictions: {country: 'usa'},
		    };

		    var autocomplete = new google.maps.places.Autocomplete(input, options);

		    // Bind the map's bounds (viewport) property to the autocomplete object,
		    // so that the autocomplete requests use the current map bounds for the
		    // bounds option in the request.
		    autocomplete.bindTo('bounds', map);

		    var infowindow = new google.maps.InfoWindow();
		    var infowindowContent = document.getElementById('infowindow-content');

		    var componentForm = {
		        street_number: 'short_name',
		        route: 'long_name',
		        locality: 'long_name',
		        administrative_area_level_1: 'short_name',
		        postal_code: 'short_name'
		    };

		    infowindow.setContent(infowindowContent);
		    var marker = new google.maps.Marker({
		      map: map,
		      anchorPoint: new google.maps.Point(0, -29)
		    });

		    autocomplete.addListener('place_changed', function() {
		      infowindow.close();
		      marker.setVisible(false);
		      var place = autocomplete.getPlace();
		      if (!place.geometry) {
		        // User entered the name of a Place that was not suggested and
		        // pressed the Enter key, or the Place Details request failed.
		        //window.alert("Address not found: '" + place.name + "'");
		        return;
		      }

		      var address = '';
		      if (place.address_components) {
		        address = [
		          (place.address_components[0] && place.address_components[0].short_name || ''),
		          (place.address_components[1] && place.address_components[1].short_name || ''),
		          (place.address_components[2] && place.address_components[2].short_name || '')
		        ].join(' ');
		      }

		        fillInAddress();

		    });

            function isInt(value) {
                return !isNaN(value) &&
                    parseInt(Number(value)) == value &&
                    !isNaN(parseInt(value, 10));
            }

		    function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();

                // check if the place has postal
                var havePostal = false;
                for( i = 0; i < place.address_components.length; i++ )
                {
                    if( place.address_components[i].types[0] == 'postal_code' ) {
                        havePostal = true;
                    }
                }

                // clear all address inputs
                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }


                if( !havePostal ) {

                    var geo = place.geometry.location,
                        location = new google.maps.LatLng(geo.lat(), geo.lng()),
                        map = new google.maps.Map(document.getElementById('map'), {
                          center: location,
                          zoom: 15
                        }),
                        request = {
                            location: location,
                            radius: '100',
                            type: ['address']
                        };

                    var service = new google.maps.places.PlacesService(map);
                    service.nearbySearch(request, firstZipCode);

                } else {

                    // Get each component of the address from the place details
                    // and fill the corresponding field on the form.
                    for (var i = 0; i < place.address_components.length; i++) {
                      var addressType = place.address_components[i].types[0];
                      if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                          document.getElementById(addressType).value = val;
                      }
                    }
                }

            }

            function firstZipCode(results, status) {

                var postalCode;

                if (status == google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        var place = results[i].geometry.location;

                        var latlng = new google.maps.LatLng(place.lat(), place.lng());

                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'latLng': latlng}, function(results, status) {

                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {


                                	// check if the loop has postal
                                	var components = results[0].address_components,
                                		hasPostal;
                                	for( c = 0; c < components.length; c++ )
                                	{
                                		if( results[0].address_components[c].types.includes('postal_code') )
                                		{
                                			hasPostal = true;
                                		}
                                	}

                                    for (j = 0; j < results[0].address_components.length; j++) {
                                        if (hasPostal)
                                        {

                                            // fill in the addresses inputs
                                            if ( results[0].address_components[j].types.length > 1 ) {

                                            	for(var t = 0;  t < results[0].address_components[j].types.length; t++)
                                            	{
                                            		var addressType = results[0].address_components[j].types[t],
                                            			val = results[0].address_components[j][componentForm[addressType]];
                                            			if( document.getElementById(addressType) )
                                            			{
                                            				document.getElementById(addressType).value = val;
                                            			}
                                            	}

                                            } else {
                                            	if( componentForm[ results[0].address_components[j].types ] )
                                            	{
                                            		var addressType = results[0].address_components[j].types;
                                            		var val = results[0].address_components[j][componentForm[addressType]];
	                                                document.getElementById(addressType).value = val;
                                            	}
                                            }

                                        }
                                    }
                                }
                            }

                        });
                    }
                }

            }

     	}

	  }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUaOvb7g_KLbkUEX-GvbFBqw6Jpgi6Cuw&libraries=places&callback=initMap" async defer></script>
<script type="text/javascript">
	var dateToday = new Date();
	$('.datepicker').datepicker({
		minDate: dateToday
	});

	function clearForm(parentElement) {
	    parentElement.find('input').not('input[type="radio"]','input[type="checkbox"]').val('');
        parentElement.find('select').val('');
	}

    if( $('input[name="list_type"]').val() == 'seller' ){
        toggleFields('seller');
    } else {
        toggleFields('buyer');
	}

	function toggleFields($type) {
        $('.form-step:not(:first-of-type)').each(function () {
            var $this = $(this),
				buyerClass = $this.data('buyer'),
				sellerClass = $this.data('seller');

            $this.removeClass('skippable');
            if ($type == 'seller') {
                if (!$this.hasClass('form-seller')) {
                    $this.addClass('skippable');
                }
				$this.removeClass(buyerClass);
                $this.addClass(sellerClass);
                $this.attr('data-sort', $this.attr('data-seller-sort')).data('sort', $this.attr('data-seller-sort'));
                $('.form-step.skippable').find('input, select').attr('disabled', 'disabled');
            } else {
                $this.removeClass(sellerClass);
                $this.addClass(buyerClass);
                $this.attr('data-sort', $this.attr('data-buyer-sort')).data('sort', $this.attr('data-buyer-sort'));
                $('.form-step').find('input, select').removeAttr('disabled', 'disabled');
            }
        });
	}

	// add list via registration form
	if( $('.form-step-client').length )
	{
		$('input[name="list_type"]').change(function()
		{
			var $registerAs = $(this).val();

			if( $registerAs == 'seller' )
			{
                clearForm($('.form-step-3'));

				// heading text
				$('.looking-to-sellbuy').hide();
			} else {
                clearForm($('.form-step-3'));

				$('.looking-to-sellbuy').show();
			}
		});
	}

	// add list from logged in user
	if( $('.form-step-added').length )
	{
        $('input[name="list_type"]').change(function(){
            getListTypeVal($(this));
        });

		function getListTypeVal($this) {
			var $registerAs = $this.val();

			if( $registerAs == 'seller' )
			{
				$('.form-step-5').addClass('disabled');
				clearForm($('.form-step-3'));

				// heading text
				$('.looking-to-sellbuy').hide();

			} else {
				$('.form-step-5').removeClass('disabled');
				clearForm($('.form-step-3'));

				$('.looking-to-sellbuy').show();
			}
		}
	}

    setTimeout(function () {
        getListTypeValGeneral($('input[name="list_type"]:checked'));
    }, 1000);

	$('input[name="list_type"]').change(function(){
        getListTypeValGeneral($(this));
	})

	function getListTypeValGeneral($this) {
        var $registerAs = $this.val();

        if( $registerAs == 'seller' )
        {

            clearForm($('.form-step-3'));

            // change the label and values of date
            $('[name="until"]')
                .html("")
                .html(
                    '<option value="Immediately">Now</option>'  +
                    '<option value="In the next two weeks">In the next two weeks</option>' +
                    '<option value="This month">This month</option>' +
                    '<option value="In more than a month">In more than a month</option>'
                );

            $(".intent").text('Where is your home located?');
            $('.intent-buy').text('sell');

            // change the heading of property
            $('.property-heading').text('What type of property are you selling?');

            // change the heading of budget
            $('.budget-heading').text('How much do you think your home is worth?');

            // change the field for budget
            $('.form-budget').html(
                '<select name="budget" class="form-control">' +
                '<option disabled selected>(e. g. $300,000)</option>' +
                '<option value="Up to $300,000" data-numeric="300000">Up to $300,000</option>' +
                '<option value="$300K - $500K" data-numeric="500000">$300K - $500K</option>' +
                '<option value="$500K - $750K" data-numeric="750000">$500K - $750K</option>' +
                '<option value="$750K - $1.0M" data-numeric="1000000">$750K - $1.0M</option>' +
                '<option value="$1.0M+" data-numeric="1000001">$1.0M+</option>' +
                '</select>'
            );

        } else {

            clearForm($('.form-step-3'));

            // change the label and values of date
            $('[name="until"]')
                .html("")
                .html(
                    '<option value="I\'m ready to make an offer">Now</option>'  +
                    '<option value="I\'m ready to start looking">I\'m ready to start looking</option>' +
                    '<option value="This month">This month</option>' +
                    '<option value="In more than a month">In more than a month</option>'
                );

            $(".intent").text('Where are you looking to buy?');
            $('.intent-buy').text('buy');

            // change the heading of property
            $('.property-heading').text('What type of property are you looking for?');

            $('.budget-heading').text('What is your budget?');

            // change the field for the budget
            $('.form-budget').html('<i class="fa fa-dollar input-group-addon"></i><input type="tel" name="budget" class="form-control validate-money" placeholder="(e. g. 300,000)">');

        }
	}


	$(document).ready(function(){
		windowResize();

		$(window).resize(function(){
			windowResize();
		});
		$(window).load(function(){
			$(window).resize();
		});
	});

	function windowResize(){
		var WindowHeight = $(window).height(),
		 	HeaderHeight = $('#header').height(),
		 	FooterHeight = $('#footer').height();

		$('#page-wrapper').each(function(){

			var ContainerHeight = $(this).outerHeight();
			if( WindowHeight > ( ContainerHeight + HeaderHeight + FooterHeight ) ) {

				TotalPadding = ( WindowHeight - ( ContainerHeight + HeaderHeight + FooterHeight ) ) / 2 ;

			} else {

				TotalPadding = 0;
			}

			$(this).css('padding-top', TotalPadding+'px').css('padding-bottom', TotalPadding+'px')
		});
	}

</script>
@stop