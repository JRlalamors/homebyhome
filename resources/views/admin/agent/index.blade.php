@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	agents dashboard
@stop

@section('page-title')
	Search Agents
@stop

<!-- Content -->
@section('content')

<section class="agents-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				{!! Form::open(['route' => 'admin.agents.search-results', 'class' => 'default-form']) !!}
				<!-- <form class="default-form" > -->
						
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-4">							
							<div class="form-group">
								<label class="control-label" for="name">Name</label>
								<div class="control-form">{!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="license">License #</label>
								<div class="control-form">{!! Form::text('license', null, ['class' => 'form-control', 'id' => 'license
								']) !!}</div>		
							</div>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="broker">Broker Name</label>
								<div class="control-form">{!! Form::text('broker', null, ['class' => 'form-control', 'id' => 'broker']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="city">City</label>
								<div class="control-form">{!! Form::text('city', null, ['class' => 'form-control', 'id' => 'city']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="state">State</label>
								<div class="control-form">{!! Form::text('state', null, ['class' => 'form-control', 'id' => 'state']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="zip">Zip</label>
								<div class="control-form">{!! Form::text('zip', null, ['class' => 'form-control', 'id' => 'zip']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="email">Email</label>
								<div class="control-form">{!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6 col-md-4">
							<div class="form-group">
								<label class="control-label" for="phone">Phone</label>
								<div class="control-form">{!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']) !!}</div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-12 col-md-4">
							<div class="form-controls">
								<button class="btn btn-pink">Search</button>
							</div>	
						</div>
					
						
					</div>
				<!-- </form> -->
				{!! Form::close() !!}



			</div>
		</div>
	</div>
</section>
<section class="agents-menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="agmenu-wrapper">
					<div class="agmenu-list equal-items">
						<div class="item">
							<a href="{{ route('admin.agents.review') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/agents/Agents-to-review.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-customerleads.svg')}})"></div>
									<h2 class="title">AGENTS TO REVIEW</h2>
								</div>
							</a>
						</div><div class="item">
							
							<a href="{{ route('admin.agents.view') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/agents/See-All-Agents.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-agentleads.svg')}})"></div>
									<h2 class="title">SEE ALL AGENTS</h2>
								</div>
							</a>

						</div><div class="item">
							<a href="{{ route('admin.agents.deals') }}" class="item-wrapper">
								<div class="bg" style="background-image:url({{asset('images/admin/agents/Curent-deals-out-for-proposal.jpg')}})"></div>
								<div class="content">
									<div class="icon" style="background-image:url({{asset('images/icon-dealsmade.svg')}})"></div>
									<h2 class="title">CURRENT DEALS OUT FOR PROPOSAL</h2>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>	
@stop


<!-- Added Scripts -->
@section('scripts')

@stop