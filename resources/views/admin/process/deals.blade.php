@extends('layouts.default')

<!-- Added Styles -->
@section('styles')
@stop

@section('body-class')
	customer customer-deals
@stop

@section('page-title')
    {{ $pageTitle }}
@stop

<!-- Content -->
@section('content')
<section id="customer-view" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="table-responsive default-table theme-blue">
                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Phone #</th>
                                <th class="text-center">Buyer?</th>
                                <th class="text-center">Seller?</th>
                            </tr>
                        </thead>
                        <tbody>                            
                            @forelse( $listings as $listing )
                                <?php if (!$listing->customer || !$listing->customer->asUser) continue; ?>
                                <tr>
                                    <td class="text-center">{{ $listing->id }}</td>
                                    <td class="text-center">{{ $listing->customer->asUser->name }}</td>
                                    <td class="text-center">{{ $listing->customer->asUser->email }}</td>
                                    <td class="text-center"><a href="tel:@phoneNum( $listing->customer->asUser->phone_num )" class="default">{{ $listing->customer->asUser->phone_format }}</a></td>
                                    <td class="text-center">{{ $listing->list_type == 'buyer' ? 'Y' : 'N' }}</td>
                                    <td class="text-center">{{ $listing->list_type == 'seller' ? 'Y' : 'N' }}</td>
                                </tr>
                            @empty
                                <tr><td colspan="10">Listings not found..</td></tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>

                <div class="pagination">
                	@if( $listings instanceof Illuminate\Pagination\LengthAwarePaginator )
                        {{ $listings->links() }}
                    @endif
                </div>

			</div>
		</div>
	</div>
</section>

@stop


<!-- Added Scripts -->
@section('scripts')

@stop