@component('mail::message', ['noFooter' => true, 'noHeader' => true])

	<table class="head-wrap">
		<tr>
			<td></td>
			<td class="header container"><table>
					<tr><br><br>
						<td><img src="https://www.homebyhome.com/images/emails/logo-homebyhome.svg"></td>
					</tr>
				</table></td>
			<td></td>
		</tr>
	</table><br><br><table class="body-wrap congrats">
		<tr>
			<td></td>
			<td class="container1"><div class="content2">
					<table>
						<tr><br><br><br><br><br>
							<td align="center"><h3>CONGRATULATIONS!</h3>
								<br>
								<br>
								<p class="cong">
									After receiving your proposal, {{ $customer->asUser->name }} has selected you to be their agent. Please contact them ASAP by phone or email. Login to HomeByHome to get their contact information now.</p>
								<a href="{{ route('login') }}" class="btn">LOGIN NOW</a><br>
								<div class="clear"></div><div class="content"><br>
									<table>
										<tr>
											<td align="center"><p> © {{ date('Y') }} HOMEBYHOME.COM </p></td>
										</tr>
										<tr>
											<td align="center"><p><a href="#"><img src="https://www.homebyhome.com/images/emails/twittericon.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/facebook.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/insta.png"></a> <a href="#"><img src="https://www.homebyhome.com/images/emails/youtube.png"></a></p></td>
										</tr>
									</table>
								</div>
							</td>
						</tr>
					</table>
				</div></td>
			<td></td>
		</tr>
	</table>

@endcomponent