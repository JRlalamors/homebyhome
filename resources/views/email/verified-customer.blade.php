@component('mail::message')
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container1"><div class="content2">
					<h3 class="beginning-text">Hi, {{ $customer->asUser->name }}</h3>
					<p>Your account has been verified and approved. Your can now use your account at homebyhome.com.</p>
					<p>For further questions, please contact us at <a href="https://www.homebyhome.com/contact-us">https://www.homebyhome.com/contact-us</a></p>
					<br>
					Thanks again,<br>
				</div></td>
			<td></td>
		</tr>
	</table>
@endcomponent