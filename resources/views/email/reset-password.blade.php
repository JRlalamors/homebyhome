@component('mail::message')
	<table class="body-wrap">
		<tr>
			<td></td>
			<td class="container"><div class="content">
				<table>
					<tr>
						<td align="center"><img src="https://www.homebyhome.com/images/emails/password-icon.png">
							<br>
							<h3>Forget your Password?</h3>
							<br>
							<p>Not to worry, we got you!<br>
								Let’s get you a new password.</p>
							<br>
							<a href="{{ url(config('app.url').route('password.reset', $token, false)) }}" class="btn2">RESET PASSWORD</a><br>
							<br>
							<hr>

							<!-- social & contact -->

							<table width="100%">
								<tr>
									<td align="center"><br>
										<br>
										<p>HomeByHome.com <br>
											can help you find a great real estate agent and save you <br>
											thousands of dollars for your next home purchase or sale.</p>
										<br>
										<a href="{{ route('buyers.process') }}" class="btn3">LEARN MORE</a> <br>
										<br>
										<span class="clear"></span></td>
								</tr>
							</table>
							<!-- /social & contact --></td>
					</tr>
				</table>
			</div></td>
		<td></td>
	</tr>
</table>
@endcomponent