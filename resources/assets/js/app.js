
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

//window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

$(document).ready(function(){
	truncText();

	var baseUrl = document.querySelectorAll("meta[name=base-url]")[0].getAttribute('content');

	// verify csrf token
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


	// Form Steps
	$.fn.formSteps = function ($option) {

		$(this).each(function(){

			$(this).find('.form-step:not(:first-of-type)')
				.addClass('hidden');

			var $formStepLength = $('.form-step').length,
                $formStepLengthAble = 0,
				$formIndex,
				$formDatas = {};

			if( $option ) {

				// Check for btns
				var $cntBTN = $option.continueBTN ? $($option.continueBTN) : $(this).find('.btn.btn-primary'),
					$backBTN = $option.backBTN ? $($option.backBTN) : $(this).find('.btn.btn-danger'),
					$loader = $option.loader ? $($option.loader) : null;

				$backBTN.hide();

				$cntBTN.click('click', function(event) {

                    $formStepLengthAble = 0;
                    $('.form-step').each(function(key){
                        if (!$(this).hasClass('skippable')) {
                            $formStepLengthAble++;
                        }
                    });

					if( $loader ) {
						$loader.show();
					}
                    var $currIndex = parseInt( $('.form-step.active').data('sort') ),
                        $formIndex = $currIndex + 1;

					/*$('.form-step').each(function($index){
						var thisIndex = parseInt( $(this).data('sort') );

						if( $(this).is(':visible') ) {
                            $formIndex = thisIndex + 1;
							/!*if( $(this).next().hasClass('disabled') ) {
								$formIndex = $(this).nextAll(':not(.disabled)').first().index() - 1;
							} else {
								$formIndex = $index + 1;
							}*!/

						}
					});*/

					if( $formStepLengthAble == $formIndex ) {
						// check for the last method
						if( $option.onLast ) {

							if( $loader ) {
								$loader.hide();
							}

							return $option.onLast();
						}
						else {
							event.preventDefault();
						}
					}
					else {

						if( $option.onContinue )
						{
							// set the keys for formdatas
							for( var $key in $option.onContinue().data ) {
								$formDatas[$key] = $option.onContinue().data[$key];
							}

							$.ajax({
								type: 'POST',
								url: $option.onContinue().url,
								data: $formDatas,
								beforeSend: function() {
									if( $loader ) {
										$loader.show();
									}
								},
								success: function($response) {

									if( $response == 'validated' )
									{

										$('.form-step').addClass('hidden').removeClass('active');
                                        $('.form-step[data-sort='+$formIndex+']').removeClass('hidden').addClass('active');
										//$('.form-step-' + $formIndex).next().removeClass('hidden').addClass('active');
										$backBTN.show();

										$('.form-ajax-notif').hide();

									} else {
										$('.form-ajax-notif').find('.alert-content ul')
												.html("")
												.end()
											.show();

										$.each($response, function(i, item) {
											$('.form-ajax-notif')
												.find('.alert-content ul')
													.append('<li>' + item  + '</li>')
										});
									}

									if( $loader ) {
										$loader.fadeOut();
									}

								}
							});

						} else {
							$('.form-step').addClass('hidden').removeClass('active');
                            $('.form-step[data-sort='+$formIndex+']').removeClass('hidden').addClass('active');
							//$('.form-step-' + $formIndex).next().removeClass('hidden').addClass('active');
							$backBTN.show();

							if( $loader ) {
								$loader.fadeOut();
							}
						}

					}

				});

				$backBTN.click(function(event){

					// unset the keys for formdata
					if( $option.onBack )
					{
						for( var $key in $option.onBack().data ) {
							delete $formDatas[$key];
						}
					}

					/*$('.form-step').each(function($index){
                        var thisIndex = parseInt( $(this).data('sort') );

						if( $(this).is(':visible') ) {

							//console.log( $(this).prevAll(':not(.disabled)').first() );
                            $formIndex = thisIndex - 1;
							/!*if( $(this).prev().hasClass('disabled') ) {
								$formIndex = $(this).prevAll(':not(.disabled)').first().index() + 1;
							}
							else {
								$formIndex = $index + 1;
							}*!/
						}
					});*/

                    var $currIndex = parseInt( $('.form-step.active').data('sort') ),
                        $formIndex = $currIndex - 1;
                    console.log($currIndex);

					if( $formIndex == 0 ) {
						$(this).hide();
						//$('.form-step[data-sort='+ ($formIndex + 1) +']').addClass('hidden').removeClass('active');
                        $('.form-step').addClass('hidden').removeClass('active');
                        $('.form-step[data-sort='+$formIndex+']').removeClass('hidden').addClass('active');
						//$('.form-step-' + $formIndex).prev().removeClass('hidden').addClass('active');
					}
					else {
						$('.form-step').addClass('hidden').removeClass('active');
                        $('.form-step[data-sort='+$formIndex+']').removeClass('hidden').addClass('active');
						//$('.form-step-' + $formIndex).prev().removeClass('hidden').addClass('active');
					}

				});


			}

		});

	}

	$.fn.numericInput = function($option) {
        $(this).each(function() {

            $(this).keydown(function(e) {

    	    	var length = $(this).val().length,
    	    		defaultLength = $option && $option.limit ? $option.limit : 14;

                var key = e.charCode || e.keyCode || 0;
                // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                // home, end, period, and numpad decimal

                return (
                    key == 8 ||
                    key == 9 ||
                    (key == 9 ||
                    key == 13 ||
                    key == 46 ||
                    key == 110 ||
                    key == 190 ||
                    (key >= 35 && key <= 40) ||
                    (key >= 48 && key <= 57) ||
                    (key >= 96 && key <= 105)) &&
                    length < defaultLength
                );

            });
        });
    };


	 $("a[data-toggle='tab']").on('shown.bs.tab', function(e) {
	      resizeWidth();
	 });

	resizeWidth();
	$('#mobile-menu').on('click touchstart','.close',function(e){
		e.preventDefault();
		$('.mobile-ctrl').click();
	});
	$(document).on('click touchstart','.mobile-ctrl', function(e){
		e.preventDefault();

		if($(this).hasClass('active')){
			$(this).removeClass('active');
			$('#mobile-menu').removeClass('active');
		}else{
			$(this).addClass('active');
			$('#mobile-menu').addClass('active');
		}

	});

	/*
	* Sweet Alert Confirmation
 	**/
	$(".confirm").click(function(e) {
		var msg = $(this).data('confirmtitle') || 'Are you sure?',
			msgText = $(this).data('confirmtext') || '',
			confirmFunc = $(this).data('confirmfunc') || false,
			confirmParams = $(this).data('confirmparams') || false;

		e.preventDefault();
		var _self = $(this);
		swal({
			title: msg,
			text: msgText,
			type: "info",
			showCancelButton: true,
			confirmButtonText: "Yes, Continue!",
			closeOnConfirm: false
		}, function(isConfirm) {
			if (isConfirm) {

				if( confirmFunc && confirmParams ) {
					// convert string into function
					var newFunc = window[confirmFunc];
					newFunc.apply(this, confirmParams);
				}
				else {
					window.location = _self.attr("href");
				}
			}
		});
	});

	$('.form-steps').formSteps({
		continueBTN: '.form-step-continue',
		backBTN: '.form-step-back',
		loader: '.form-loader-wrap',
		onContinue: function(url, data) {

			var $formDatas = {};

			$('.form-step.active').find('input').each(function() {
				$formDatas[ $(this).attr('name') ] = $(this).val();
			});

			return { url: baseUrl + '/agent/validate-form', data: $formDatas };
		},
		onBack: function() {

			var $formDatas = {};

			$('.form-step.active').find('input').each(function() {
				$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
			});

			return { data: $formDatas }

		},
		onLast: function() {

			var $data = {
				first_name: $('input[name="first_name"]').val(),
				last_name : $('input[name="last_name"]').val(),
				email : $('input[name="email"]').val(),
				phone_num : $('input[name="phone_num"]').val(),
				current_role : $('input[name="current_role"]:checked').val() == 'other' ? $('input[name="other_role"]').val() : $('input[name="current_role"]:checked').val(),
				work_with : $('input[name="work_with"]:checked').val(),
				zillow_url : $('input[name="zillow_url"]').val(),
				linkedin_url : $('input[name="linkedin_url"]').val(),
                facebook_url : $('input[name="facebook_url"]').val(),
                yelp_url : $('input[name="yelp_url"]').val(),
				website_url : $('input[name="website_url"]').val()
			};

			$.ajax({
				type: 'POST',
				url: baseUrl + '/agent/submit-registration',
				data: $data,
				success: function ($response) {

					if( $response == 'added' ) {
						window.location.href = baseUrl + '/agent/register-success';
					}
					else {
						$('.form-ajax-notif')
							.find('.alert-content ul')
								.html("")
								.end()
							.show();

						$.each($response, function(i, item) {
							$('.form-ajax-notif')
								.find('.alert-content ul')
									.append('<li>' + item  + '</li>')
						});
					}
				}
			});
		}
	});

	$('.form-step-client').formSteps({
		continueBTN: '.form-step-continue',
		backBTN: '.form-step-back',
		loader: '.form-loader-wrap',
		onContinue: function(url, data) {

			var $formDatas = {};

			$('.form-step.active').find('input, select').not('[disabled]').each(function() {

				if( $(this).attr('name') == 'list_type' )
				{
					if( $(this).is(':checked') )
					{
						$formDatas[ $(this).attr('name') ] = $(this).val();
					}
				} else {
					$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
				}
			});

			return { url: baseUrl + '/register/validate-form', data: $formDatas };

		},
		onBack: function() {

			var $formDatas = {};

			$('.form-step.active').find('input, select').each(function() {
				$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
			});

			return { data: $formDatas };

		},
		onLast: function() {
			var $data = {
				'list_type' : $('input[name="list_type"]:checked').val(),
				'first_name' : $('input[name="first_name"]').val(),
				'last_name' : $('input[name="last_name"]').val(),
				'phone_num' : $('input[name="phone_num"]').val(),
				'email' : $('input[name="email"]').val(),
				'password' : $('input[name="password"]').val(),
				'property_type_id' : $('input[name="property_type_id"]:checked').val(),
				'zip_postal' : $('input[name="zip_postal"]').val(),
				'budget' : $('[name="budget"]').val(),
                'numeric_budget' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('[name="budget"]').val() : $('[name="budget"]').find('option:selected').data('numeric'),
				'status_to_lender' : $('input[name="status_to_lender"]:checked').val(),
				'sq_feet_min' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('input[name="sq_feet_min"]').val() : null,
				'sq_feet_max' : $('input[name="list_type"]:checked').val() == 'buyer' ?  $('input[name="sq_feet_max"]').val() : null,
				'bedrooms' : $('input[name="list_type"]:checked').val() == 'buyer' ?  $('select[name="bedrooms"]').val() : null,
				'bathrooms' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('select[name="bathrooms"]').val() : null,
				'city' : $('input[name="city"]').val(),
				'state' : $('input[name="state"]').val(),
				'street_address' : $('input[name="street_address"]').val(),
				'unit_no' : $('input[name="unit_no"]').val(),
				'until' : $('select[name="until"]').val()
			};

			console.log( $data );

			$.ajax({
				type: 'POST',
				url: baseUrl + '/register/store',
				data: $data,
				success: function($response) {
					if( $response.message == 'added' )
					{
						window.location.href = baseUrl + '/register/success/' + $response.user.id;
					} else {
						$('.form-ajax-notif')
							.find('.alert-content ul')
								.html("")
								.end()
							.show();

						$.each($response, function(i, item) {
							$('.form-ajax-notif')
								.find('.alert-content ul')
									.append('<li>' + item  + '</li>')
						});
					}
				}
			});

		}
	});

	$('.form-step-addlist').formSteps({
		continueBTN: '.form-step-continue',
		backBTN: '.form-step-back',
		loader: '.form-loader-wrap',
		onContinue: function(url, data) {

			var $formDatas = {};

			$('input[type="hidden"]').each(function(){
				$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
			});

			$('.form-step.active').find('input, select').each(function() {

				if( $(this).attr('name') == 'list_type' )
				{
					if( $(this).is(':checked') )
					{
						$formDatas[ $(this).attr('name') ] = $(this).val();
					}
				} else {
					$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
				}

			});

			return { url: baseUrl + '/customer/list/validate-form', data: $formDatas };

		},
		onBack: function() {

			var $formDatas = {};

			$('.form-step.active').find('input, select').each(function() {
				$formDatas[ $(this).attr('name') ] = $(this).val() == null ? '' : $(this).val();
			});

			return { data: $formDatas }

		},
		onLast: function() {

			var $data = {
				'list_type' : ($('.form-step-added').length ? $('input[name="list_type"]:checked').val() : $('input[name="list_type"]').val()),
				'property_type_id' : $('input[name="property_type_id"]:checked').val(),
				'zip_postal' : $('input[name="zip_postal"]').val(),
				'budget' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('input[name="budget"]').val() : $('select[name="budget"]').val(),
                'numeric_budget' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('[name="budget"]').val() : $('[name="budget"]').find('option:selected').data('numeric'),
				'status_to_lender' : $('input[name="status_to_lender"]:checked').val(),
				'sq_feet_min' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('input[name="sq_feet_min"]').val() : null,
				'sq_feet_max' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('input[name="sq_feet_max"]').val() : null,
				'bedrooms' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('select[name="bedrooms"]').val() : null,
				'bathrooms' : $('input[name="list_type"]:checked').val() == 'buyer' ? $('select[name="bathrooms"]').val() : null,
				'city' : $('input[name="city"]').val(),
				'state' : $('input[name="state"]').val(),
				'street_address' : $('input[name="street_address"]').val(),
				'unit_no' : $('input[name="unit_no"]').val(),
				'until' : $('select[name="until"]').val(),
			};

			$.ajax({
				type: 'POST',
				url: baseUrl + '/customer/list/store',
				data: $data,
				success: function($response) {
					if( $response == 'added' )
					{
						window.location.href = baseUrl + '/customer/account';
					} else {
						$('.form-ajax-notif')
							.find('.alert-content ul')
								.html("")
								.end()
							.show();

						$.each($response, function(i, item) {
							$('.form-ajax-notif')
								.find('.alert-content ul')
									.append('<li>' + item  + '</li>')
						});
					}
				}
			});

		}
	});

	Array.prototype.chunk = function ( n ) {
	    if ( !this.length ) {
	        return [];
	    }
	    return [ this.slice( 0, n ) ].concat( this.slice(n).chunk(n) );
	};

	// numeric input
	$('.validate-phone-num').keydown(function(e) {

		var $val = $(this).val(),
			$key = e.charCode || e.keyCode || 0;

		$val = $val.replace(/\)/, "");
		$val = $val.replace(/\(/, "");
		$val = $val.replace(/\-/, "");
		$val = $val.replace(" ", "");

		$val = $val.split("");
		$val = $val.chunk(3);

		if( $val[0] ) {
			$val[0] = '(' + $val[0].join('') + ') ';
		}

		if( $val[1] ) {
        	$val[1] = $val[1].join('') + ($val.length > 2 ? '-' : '');
		}

		if( $val[2] ) {
			$val[2] = $val[2].join('');
		}

		$val = $val.join('');

		if( $key != 8 ) {
			$(this).val($val);
		}

	}).numericInput();

	// money formatting
	/*$('.form-budget').on('keyup', '.validate-money', function(e){

		var $val = $(this).val(),
			$key = e.charCode || e.keyCode || 0;

			$val = $val.replace('$', "");
			$val = $val.replace(/,/g, "");

			$val = numeral($val).format('$0,0');

			$(this).val($val);

	});*/

	$(window).scroll(function(){
		resizeWidth();

	});
	$(window).resize(function(){
		testiCarousel();
		resizeWidth();
		$('.truncate_p').trigger("update");
	});
	$(window).load(function(){
		$(window).resize();

		$('.main-loading').fadeOut();
	});

});
function resizeWidth() {

	$(".equal-items .item-wrapper").matchHeight({'byRow':false});

	positionFooter();
}

 function positionFooter() {


 	if(!$('body').hasClass('footer-off')){
	   	var footerHeight = 0,
	       footerTop = 0,
	       $footer = $("#footer");

	    footerHeight = $footer.outerHeight();
	    footerTop = ($(window).scrollTop()+$(window).height()-footerHeight);

	  if ( ($('body').height()+footerHeight) < $(window).height()) {
	       $footer.css({
	            position: "absolute"
	       })
	   } else {
	       $footer.css({
	            position: "static"
	       })
	   }
	}

}
function testiCarousel(){

	if($('.tst-list').length){

		if(Modernizr.mq('(min-width: 992px)')){
			var itemWidth = 380;
		}else{
			var itemWidth = 280;
		}
		$('.tst-list').carouFredSel({
			responsive: false,
			scroll: 1,
			width: '100%',
			height: 'variable',
			items: {
				height: 'variable',
				width: itemWidth,
				visible: {
					min: 1,
					max: 3
				}
			},
			onCreate : function () {
			}
		});

		$(".tst-list .content").matchHeight();
	}
}

function truncText(){

	$('.truncate_p').each(function(){

		var pLines = $(this).data('paragraph');
		var lineHeight = parseInt($(this).css('lineHeight'),10);
		var fontSize = parseInt($(this).css('font-size'),10);
		var divHeight = ( lineHeight * pLines ) + pLines;

		$(this).dotdotdot({
	      	height : divHeight
	    });

	});
}

/*
* global scope functions 
**/

// Approving an agent
window.approveAgent = function($agentID) {

	$.ajax({
		type: 'POST',
		url: baseUrl + '/admin/agents/' + $agentID + '/approve',
		success: function($data) {
			if( $data.message == 'success' )
			{
				swal({
					title: 'Approved',
					text: 'Agent has been approved.',
					type: 'success'
				}, function(){
					window.location = baseUrl + '/admin/agents/' + $agentID + '/profile/approved';
				});
			} else {
				swal('Error', 'Something went wrong', 'error');
			}
		}
	});

}


// rejecting an agent
window.rejectAgent = function($agentID) {

	$.ajax({
		type: 'POST',
		url: baseUrl + '/admin/agents/' + $agentID + '/reject',
		success: function($data) {
			if( $data.message == 'success' )
			{
				swal({
					title: 'Agent',
					text: 'Agent has been rejected.',
					type: 'success'
				}, function(){
					window.location = baseUrl + '/admin/agents/' + $agentID + '/profile/rejected';
				});
			} else {
				swal('Error', 'Something went wrong', 'error');
			}
		}
	});

}

// Approving a customer
window.approveCustomer = function($customerID) {

	$.ajax({
		type: 'POST',
		url: baseUrl + '/admin/customers/' + $customerID + '/approve',
		success: function($data) {
			if( $data.message == 'success' )
			{
				swal({
					title: 'Approved',
					text: 'Customer has been approved.',
					type: 'success'
				}, function(){
					window.location = baseUrl + '/admin/customers/' + $customerID + '/profile/approved';
				});
			} else {
				swal('Error', 'Something went wrong', 'error');
			}
		}
	});

}

// Rejecting a customer
window.rejectCustomer = function($customerID) {

	$.ajax({
		type: 'POST',
		url: baseUrl + '/admin/customers/' + $customerID + '/reject',
		success: function($data) {
			if( $data.message == 'success' )
			{
				swal({
					title: 'Rejected',
					text: 'Customer has been rejected.',
					type: 'success'
				}, function(){
					window.location = baseUrl + '/admin/customers/' + $customerID + '/profile/rejected';
				});
			} else {
				swal('Error', 'Something went wrong', 'error');
			}
		}
	});

}

// Resize reCAPTCHA to fit width of container
// Since it has a fixed width, we're scaling
// using CSS3 transforms
// ------------------------------------------
// captchaScale = containerWidth / elementWidth

function scaleCaptcha(elementWidth) {
  // Width of the reCAPTCHA element, in pixels
  var reCaptchaWidth = 304;
  // Get the containing element's width
	var containerWidth = $('.recaptcha-form').width();

  // Only scale the reCAPTCHA if it won't fit
  // inside the container
  if(reCaptchaWidth > containerWidth) {
    // Calculate the scale
    var captchaScale = containerWidth / reCaptchaWidth;
    // Apply the transformation
    $('.g-recaptcha').css({
      'transform':'scale('+captchaScale+')'
    });
  }
}

$(document).ready(function(){
  // Initialize scaling
  scaleCaptcha();

  // Update scaling on window resize
  // Uses jQuery throttle plugin to limit strain on the browser
  $(window).resize( function(){
  	scaleCaptcha();
  });


});