

<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		.photo img {
			display: inline-block;
		}

		.panel-group a {
			display: block;			
			padding: 20px 15px;
		}

		.panel-group a:hover, 
		.panel-group a:focus {
			text-decoration: none;
		}

		.panel-group .panel-heading {
			padding: 0;
		}
	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer-profile pagetitle-off
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section id="customer-profile" class="default-section">
	<?php echo Form::model($authUser, ['route' => 'customer.profile.update', 'class' => 'default-form', 'files' => true]); ?>

		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-4">
					<div class="photo text-center">
						<img class="img-responsive" src="<?php echo e($authUser->profile_photo ? $authUser->profile_photo : asset('images/user-blank.svg')); ?>">
					</div>

					<div class="form-edit <?php echo e(count($errors) == 0 ? 'hidden' : ''); ?>">
						<div class="form-group">
							<br>
							<?php echo e(Form::file('photo', ['class' => 'form-control'])); ?>

						</div>
					</div>
				</div>
				<div class="col-xs-12 col-md-8">
					<div class="info">
						<h1 class="name"><?php echo e($authUser->name); ?></h1>
						
						<?php if( $authUser->city ): ?>
							<div class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo e($authUser->city . " " . $authUser->state); ?></div>
						<?php endif; ?>

						<div class="moreinfo">
							<div class="listitem">
								<div class="title">Email</div><div class="content"><a href="mailto:<?php echo e($authUser->email); ?>"><?php echo e($authUser->email); ?></a></div>
							</div>
							
							<?php if( $authUser->city ): ?>
								<div class="listitem">
									<div class="title">City</div><div class="content"><?php echo e($authUser->city); ?></div>
								</div>
							<?php endif; ?>

							<?php if( $authUser->state ): ?>
								<div class="listitem">
									<div class="title">State</div><div class="content"><?php echo e($authUser->state); ?></div>
								</div>
							<?php endif; ?>
							
							<div class="listitem">
								<div class="title">Phone</div><div class="content"><a href="tel:<?php echo preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $authUser->phone_num) ?>"><?php echo e($authUser->phone_format); ?></a></div>
							</div>
							
							<div class="ctrls">
								<a href="javascript:;" class="btn btn-red btn-round hvr-rectangle-out edit-profile <?php echo e(count($errors) > 0 ? 'hidden' : ''); ?>">Edit</a>
							</div>
						</div>					
					</div>

					<div class="form-edit <?php echo e(count($errors) == 0 ? 'hidden' : ''); ?>">							
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<label>First Name</label>
									<?php echo Form::text('first_name', null, ['class' => 'form-control']); ?>

								</div>
								<div class="col-xs-6">
									<label>Last Name</label>
									<?php echo Form::text('last_name', null, ['class' => 'form-control']); ?>

								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="row">
								<div class="col-xs-12 col-sm-7">
									<label>City</label>
									<?php echo Form::text('city', null, ['class' => 'form-control autocomplete', 'id' => 'locality']); ?>

								</div>

								<div class="col-xs-12 col-sm-5">
									<label>State</label>
									<?php echo Form::text('state', null, ['class' => 'form-control autocomplete', 'id' => 'administrative_area_level_1']); ?>

								</div>

								<div id="map" class="hidden"></div>
							</div>
						</div>

						<div class="form-group">
							<label>Phone #</label>
							<?php echo Form::text('phone_num', $authUser->phone_format, ['class' => 'form-control validate-phone-num']); ?>

						</div>
						<div class="form-group ctrls">
							<button class="btn btn-lblue btn-round hvr-rectangle-out">Update</button>
							<a href="javascript:;" class="btn btn-red btn-round hvr-rectangle-out cancel-edit">Cancel</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php echo Form::close(); ?>


	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<br>
				<?php echo $__env->make('partials.form-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>	
			</div>
		</div>
	</div>

</section>
<section id="customer-moreinfo" class="default-section">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8 col-md-push-4">
				<div class="panel-group" id="accordion">
					<?php $__empty_1 = true; $__currentLoopData = $lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo e($list->id); ?>"><?php echo e($list->property->human_display); ?></a>
								</h4>
							</div>
							<div id="collapse<?php echo e($list->id); ?>" class="panel-collapse collapse <?php echo e($loop->first ? 'in' : ''); ?>">
								<div class="panel-body profile cp-container">
									<div class="listitem">
										<div class="title">Are you looking to buy a home or sell a home?</div>
										<div class="content"><?php echo e($list->list_type == 'buyer' ? 'Buy Home' : 'Sell Home'); ?></div>
									</div>
									<div class="listitem">
										<div class="title">Where are you looking to buy / sell?</div>
										<div class="content"><?php echo e($list->complete_address); ?></div>
									</div>
									<div class="listitem">
										<div class="title">Home much are you hoping to spend/sell on a home?</div>
										<div class="content">
											<?php if( $list->text_budget ): ?>
												<?php echo e($list->text_budget); ?>

											<?php elseif( $list->budget ): ?>
												<?php echo e($list->budget); ?>

											<?php else: ?>
												<?php echo e('N/A'); ?>

											<?php endif; ?>
										</div>
									</div>
									<div class="listitem">
										<div class="title">What kind of property are you buying/selling?</div>
										<div class="content"><?php echo e($list->property->human_display); ?></div>
									</div>
									<div class="listitem">
										<div class="title">How soon are you looking to buy/sell?</div>
										<div class="content">
											<?php if($list->text_until): ?>
												<?php echo e($list->text_until); ?>

											<?php else: ?>
												<?php echo date('M d, o', strtotime($list->until)); ?>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
						<div class="profile cp-container">	
							<p class="lead">Lists not found..</p>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-xs-12 col-md-4 col-md-pull-8">
				<div class="sidebar cp-container">
					<div class="listitem">
						<div class="title">Date Signed Up</div>
						<div class="content"><?php echo date('M d, o', strtotime($authUser->created_at)); ?></div>
					</div>
					<div class="listitem">
						<div class="title">Have They Selected An Agent</div>
						<div class="content">Yes</div>
					</div>
					<div class="listitem">
						<div class="title">Proposal Submitted</div>
						<div class="content">September 10, 2017</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script>
      function initMap() {
        // return;
        var map = new google.maps.Map(document.getElementById('map'));

        var input = document.getElementById('locality');

        var options = {
          	types: ['geocode'],
          	componentRestrictions: {country: 'usa'},        
        };

        var autocomplete = new google.maps.places.Autocomplete(input, options);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');

        var componentForm = {
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
        };

        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
          map: map,
          anchorPoint: new google.maps.Point(0, -29)
        });

        autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("Address not found: '" + place.name + "'");
            return;
          }

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }

            fillInAddress();
        });

        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();

            for (var component in componentForm) {
              document.getElementById(component).value = '';
              document.getElementById(component).disabled = false;
            }

            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
              var addressType = place.address_components[i].types[0];
              if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
              }
            }
        }

      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAUaOvb7g_KLbkUEX-GvbFBqw6Jpgi6Cuw&libraries=places&callback=initMap"
            async defer></script>
<script type="text/javascript">
	$(document).ready(function(){

		$('.edit-profile, .cancel-edit').click(function(){
			$('.edit-profile').toggleClass('hidden');
			$('.form-edit').toggleClass('hidden');
			$('.info').toggleClass('hidden');
		});

		// $('.cancel-edit').click(function() {
		// 	$('.form-edit').addClass('hidden');
		// 	$('.edit-profile').removeClass('hidden');
		// });

		$('#locality').keypress(function(e) {

			var key = e.charCode || e.keyCode || 0;

			if( key == 13 )
			{
				e.preventDefault();
			}
		});

	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>