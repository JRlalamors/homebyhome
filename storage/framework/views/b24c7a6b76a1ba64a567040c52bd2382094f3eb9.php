<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap">
        <tr>
            <td></td>
            <td class="container1"><div class="content2">
                    <h3 class="beginning-text">New Agent Registration!</h3>
                    <p><label>Name: </label> <?php echo e($agent->asUser->name); ?></p>
                    <p><label>Email: </label> <?php echo e($agent->asUser->email); ?></p>
                    <p><label>Phone #: </label> <?php echo e($agent->asUser->phone_num); ?></p>
                    <br>
                    Thanks again,<br>
                </div></td>
            <td></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>