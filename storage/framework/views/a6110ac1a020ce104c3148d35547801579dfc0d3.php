

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<style type="text/css">
    .prmenu-wrapper .nav-tabs {
        border: 0;
    }
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('body-class'); ?>
    dashboard
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-title'); ?>
    Dashboard
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section class="main-dashboard">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="prmenu-wrapper">
                    <div class="prmenu-list equal-items nav nav-tabs row">
                        <div class="item active col-xs-12 col-sm-4">
                            <a href="#tab-customers" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url(<?php echo e(asset('images/admin/dashboard/customer-leads.jpg')); ?>)"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url(<?php echo e(asset('images/icon-agentsreview.svg')); ?>)"></div>
                                    <h2 class="title">CUSTOMER LEADS</h2>
                                </div>
                            </a>
                        </div>

                        <div class="item col-xs-12 col-sm-4">                            
                            <a href="#tab-agents" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url(<?php echo e(asset('images/admin/dashboard/agent-leads.jpg')); ?>)"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url(<?php echo e(asset('images/icon-agentleads.svg')); ?>)"></div>
                                    <h2 class="title">AGENT LEADS</h2>
                                </div>
                            </a>
                        </div>

                        <div class="item col-xs-12 col-sm-4">
                            <a href="#tab-deals" class="item-wrapper" data-toggle="tab">
                                <div class="bg" style="background-image:url(<?php echo e(asset('images/admin/dashboard/deals-made.jpg')); ?>)"></div>
                                <div class="content">
                                    <div class="icon" style="background-image:url(<?php echo e(asset('images/icon-dealsmade.svg')); ?>)"></div>
                                    <h2 class="title">DEALS MADE</h2>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

               <div class="tab-content">
                   <div id="tab-customers" class="tab-pane active">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Customer ID</th>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Phone #</th>
                                            <th class="text-center">Buyer?</th>
                                            <th class="text-center">Seller?</th>
                                            <th class="text-center">View Full Profile</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $__empty_1 = true; $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <tr>
                                                <td class="text-center"><?php echo e($customer->id); ?></td>
                                                <td class="text-center"><?php echo e($customer->asUser->name); ?></td>
                                                <td class="text-center"><?php echo e($customer->asUser->email); ?></td>
                                                <td class="text-center"><?php echo e($customer->asUser->phone_format); ?></td>
                                                <td class="text-center"><?php echo e($customer->buyerList()->count() > 0 ? 'Y' : 'N'); ?></td>
                                                <td class="text-center"><?php echo e($customer->sellerList()->count() > 0 ? 'Y' : 'N'); ?></td>
                                                <td class="text-center"><a href="<?php echo e(route('admin.customers.profile', $customer)); ?>" class="blue">View Profile</a></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <tr>
                                                <td colspan="7">Customers not found..</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>

                   <div id="tab-agents" class="tab-pane fade">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Name</th>
                                            <th class="text-center">Email</th>
                                            <th class="text-center">Agency</th>
                                            <th class="text-center">Phone #</th>
                                            <th class="text-center">View Full Profile</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $__empty_1 = true; $__currentLoopData = $agents; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agent): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <tr>
                                                <td class="text-center"><?php echo e($agent->asUser->name); ?></td>
                                                <td class="text-center"><?php echo e($agent->asUser->email); ?></td>
                                                <td class="text-center"><?php echo e($agent->zillowApi()['proInfo']['businessName']); ?></td>
                                                <td class="text-center"><?php echo e($agent->asUser->phone_format); ?></td>
                                                <td class="text-center"><a href="<?php echo e(route('admin.agents.profile', $agent)); ?>" class="blue">View Profile</a></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <tr>
                                                <td colspan="5">Agents not found..</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>

                   <div id="tab-deals" class="tab-pane fade">
                       <div class="details-wrapper">
                            <div class="table-responsive default-table theme-blue">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Transaction ID</th>
                                            <th class="text-center">Date Scheduled</th>
                                            <th class="text-center">Client's Name</th>
                                            <th class="text-center">Rebate</th>
                                            <th class="text-center">Deal Closed</th>
                                            <th class="text-center">View Details</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $__empty_1 = true; $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                                            <tr>
                                                <td class="text-center"><?php echo e($deal->id); ?></td>
                                                <td class="text-center"><?php echo date('M d, o', strtotime($deal->until)); ?></td>
                                                <td class="text-center"><?php echo e($deal->customer->asUser->name); ?></td>
                                                <td class="text-center"><?php echo e($deal->selectedProposal() ? $deal->selectedProposal()->rebate_format : '
                                                '); ?></td>
                                                <td class="text-center"><?php echo e($deal->closed_deal ? 'Y' : 'N'); ?></td>
                                                <td class="text-center"><a href="<?php echo e(route('admin.deals.details', $deal)); ?>" class="blue">View Details</a></td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                                            <tr>
                                                <td colspan="6">Agents not found..</td>
                                            </tr>
                                        <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                       </div>
                   </div>
               </div>

            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>