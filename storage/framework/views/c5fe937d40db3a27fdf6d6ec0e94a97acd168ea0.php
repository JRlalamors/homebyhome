<?php $__env->startSection('afteElements'); ?>
<div id="customerList" class="modal fade modal-edit-list" role="dialog">
	<div id="ajax-result" class="modal-dialog">
	</div>
</div>
<div id="assignAgent" class="modal fade" role="dialog">
	<div class="modal-dialog">
	    <!-- Modal content-->
	    <div class="modal-content">
	      	<div class="modal-header">
	        	<button type="button" class="close" data-dismiss="modal">&times;</button>
	        	<h4 class="modal-title">Assign Agents for <?php echo e(isset( $customer ) ? $customer->asUser->name : ''); ?></h4>
	      	</div>
	      	<div class="modal-body">
	      		<div class="form default-form">      			
		      		<div class="form-group">
		      			<div class="row">  				
			      			<div class="col-xs-12 col-sm-4">Assign Agent #1</div>	
			      			<div class="col-xs-12 col-sm-8">
			      				<select name="agent-1" class="form-control agents-select">
			      				</select>
			      			</div>	
		      			</div>
		      		</div>
		      		<div class="form-group">
		      			<div class="row">      				
			      			<div class="col-xs-12 col-sm-4">Assign Agent #2</div>	
			      			<div class="col-xs-12 col-sm-8">
			      				<select name="agent-2" class="form-control agents-select">
			      				</select>
			      			</div>
		      			</div>	
		      		</div>
		      		<div class="form-group">
		      			<div class="row">      				
			      			<div class="col-xs-12 col-sm-4">Assign Agent #3</div>	
			      			<div class="col-xs-12 col-sm-8">
			      				<select name="agent-3" class="form-control agents-select">
			      				</select>
			      			</div>	
		      			</div>
		      		</div>	
		      		<?php echo $__env->make('partials.form-ajax-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	      		</div>	
	      	</div>
		    <div class="modal-footer">
		    	<button type="button" class="btn btn-primary set-agent">Set Agents</button>
		    </div>
	    </div>
	</div>
</div>
<?php $__env->stopSection(); ?>

