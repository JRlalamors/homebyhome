

<?php $__env->startSection('styles'); ?>
<style type="text/css">
	
	.experience-areas {
		margin-bottom: 40px;
		border-bottom: 2px solid #1FC0DA;
		padding-bottom: 30px;
	}

	.areas-serviced {
		margin: 30px 0 0;
	}

	.areas-serviced li {
		margin: 0 0 10px;
		color: #727272;
		padding-left: 30px;
	}

	body.agent-proposal #proposal-profile .profile .promo .dc.fixed {
		font-size: 45px;
		margin: 25px 0;
	}

</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agent-proposal site-notice
<?php $__env->stopSection(); ?>
<?php $__env->startSection('page-title'); ?>
	<?php echo e($agent->asUser->name); ?>'s Proposal for <?php echo e($customer); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<section id="proposal-profile">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div class="profile">
					<div class="item">
						<div class="photo">
							<?php if( $agent->asUser->profile_photo ): ?>
								<img class="img-responsive" src="<?php echo e($agent->asUser->profile_photo); ?>">
							<?php else: ?>
								<img class="img-responsive" src="<?php echo e(isset( $zillowApi['proInfo']['photo'] ) ? $zillowApi['proInfo']['photo'] : asset('images/user-blank.svg')); ?>">
							<?php endif; ?>
						</div>
					</div><div class="item">
						<h2 class="name"><?php echo e($agent->asUser->name); ?></h2>
						<div class="company">
							<?php echo e($zillowApi != null ? $zillowApi['proInfo']['businessName'] : ''); ?> <br>

							<?php echo e($agent->asUser->email); ?> <br>

							<?php echo e(isset( $zillowApi['proInfo']['phone'] ) ? $zillowApi['proInfo']['phone'] : $agent->phone_num); ?>

						</div>

						<div class="company">
							<ul class="list-inline">
								<?php if($agent->zillow_url): ?>
									<li>
										<a href="<?php echo e($agent->zillow_username); ?>" target="_blank">
											<img src="<?php echo e(asset('images/zillow-icon.png')); ?>">
										</a>
									</li>
								<?php endif; ?>
								<?php if($agent->linkedin_url): ?>
									<li>
										<a href="<?php echo e($agent->linkedin_url); ?>" target="_blank">
											<img src="<?php echo e(asset('images/linkedin-icon.png')); ?>">
										</a>
									</li>
								<?php endif; ?>
								<?php if($agent->website_url): ?>
									<li>
										<a href="<?php echo e($agent->website_url); ?>" target="_blank">
											<img src="<?php echo e(asset('images/earth-icon.png')); ?>">
										</a>
									</li>
								<?php endif; ?>
								<?php if($agent->getOtherData('facebook_url')): ?>
									<li>
										<a href="<?php echo e($agent->getOtherData('facebook_url')); ?>" target="_blank">
											<img src="<?php echo e(asset('images/facebook-icon.png')); ?>">
										</a>
									</li>
								<?php endif; ?>
								<?php if($agent->getOtherData('yelp_url')): ?>
									<li>
										<a href="<?php echo e($agent->getOtherData('yelp_url')); ?>" target="_blank">
											<img src="<?php echo e(asset('images/yelp-icon.png')); ?>">
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
						
						<div class="rating">
							<?php 
								$avgRating = $zillowApi['proInfo']['avgRating'] ? intval($zillowApi['proInfo']['avgRating']) : 0;	
								$remaining = 5 - $avgRating;

								for( $i = 1; $i <= $avgRating; $i++ ) 
								{
									echo '<i class="fa fa-star" aria-hidden="true"></i> ';
								}

								if( fmod($avgRating, 1) !== 0.00 ) 
								{
									echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
								}

								for( $i = 1; $i <= $remaining; $i++ ) 
								{
									echo '<i class="fa fa-star-o" aria-hidden="true"></i> ';
								}
							 ?>
						</div>
						<div class="ctrls">
							<a href="javascript:;" class="btn btn-block btn-blue  " data-toggle="modal" data-target="#contactMe">Contact Me</a>

							<?php if( !$list->hasSelectedAProposal() ): ?>
								<a href="<?php echo e(route('customer.list.agent.hire', [$list, $agent])); ?>" class="btn btn-block btn-pink   confirm" data-confirmtitle="Hire Agent?" data-confirmtext="Are you sure you want to hire this agent?">Hire Me</a>
							<?php endif; ?>
						</div>
					</div><div class="item">
						<div class="promo">
							<div class="dc <?php echo e($proposal->rebate_type == 'fixed' ? 'fixed' : ''); ?>">
								<?php echo e($proposal->rebate_format); ?>

							</div>
							<?php if($list->isBuyer()): ?>
								<div class="dc-text">Agent commission rebate.</div>
								<?php if($proposal->rebate_type == 'fixed'): ?>
									<div class="dc-text">
										Save <?php echo e($proposal->approximate); ?> when you use <?php echo e($agent->asUser->first_name); ?> to buy a home.
									</div>
								<?php else: ?>
									<div class="dc-text">
										Save approximately <br><?php echo e($proposal->approximate); ?> when you use <?php echo e($agent->asUser->first_name); ?> to buy a home.
									</div>
								<?php endif; ?>
							<?php else: ?>
								<div class="dc-text">Listing commission rate.</div>
								<div class="dc-text">
									Save approximately <br><?php echo e($proposal->approximate); ?> when you use <?php echo e($agent->asUser->first_name); ?> to sell your home.
								</div>
							<?php endif; ?>

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="proposal-info" class="default-section">

	<div class="container experience-areas">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<h2 class="heading">Experience Levels</h2>
				<div class="textarea list-satisfaction">
				 	<div class="listitem">
				 		<div class="title">Local Knowledge</div><div class="content"><?php echo e($agent->ratingPercentage('localknowledgeRating')); ?> Satisfaction</div>
				 	</div>
				 	<div class="listitem">
				 		<div class="title">Process Expertise</div><div class="content"><?php echo e($agent->ratingPercentage('processexpertiseRating')); ?> Satisfaction</div>
				 	</div>
				 	<div class="listitem">
				 		<div class="title">Negotiations</div><div class="content"><?php echo e($agent->ratingPercentage('negotiationskillsRating')); ?> Satisfaction</div>
				 	</div>
				 	<div class="listitem">	
				 		<div class="title">Responsiveness</div><div class="content"><?php echo e($agent->ratingPercentage('responsivenessRating')); ?> Satisfaction</div>
				 	</div>
			 	</div>
			</div>

			<div class="col-xs-12 col-sm-6">
				<h2 class="heading">Areas Serviced</h2>
				<?php if( isset($agent->zillowApi()['proInfo']['serviceAreas']['area']) ): ?>
				
					<ul class="list-unstyled areas-serviced" style="display: inline-block">
						<?php $__currentLoopData = $agent->zillowApi()['proInfo']['serviceAreas']['area']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $area): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php if($loop->iteration % 5 == 0): ?>
								</ul><ul class="list-unstyled areas-serviced" style="display: inline-block">
							<?php endif; ?>
							<li><strong><?php echo e($area); ?></strong></li>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</ul>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			 <div class="col-xs-12">
			 	<h2 class="heading">Why Should You Hire Me?</h2>
			 	<div class="textarea">
		 			<?php echo e($proposal->getOtherData('propose_1') ? $proposal->getOtherData('propose_1') : $agent->getOtherData('question_1')); ?> 
			 	</div>

			 	<h2 class="heading">What Do I Do Better Than Most Agents?</h2>
			 	<div class="textarea">
		 			<?php echo e($proposal->getOtherData('propose_2') ? $proposal->getOtherData('propose_2') : $agent->getOtherData('question_2')); ?> 
			 	</div>

			 	<h2 class="heading">What Do Others Say About Me?</h2>			 	
			 	<?php if( isset($zillowApi['proReviews']['review']) && count($zillowApi['proReviews']['review']) ): ?>
					<div class="cs-list">
						<ul>
							<?php $__currentLoopData = $zillowApi['proReviews']['review']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $review): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li>
									<div class="cs-item">
										<div class="top">
											<div class="stars">
												<?php 
													$r1 = floatval( isset($review['localknowledgeRating']) ? $review['localknowledgeRating'] : 0 );
													$r2 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
													$r3 = floatval( isset($review['processexpertiseRating']) ? $review['processexpertiseRating'] : 0 );
													$r4 = floatval( isset($review['negotiationskillsRating']) ? $review['negotiationskillsRating'] : 0 );

													$raRating = ($r1 + $r2 + $r3 + $r4) / 4;

													$remaining = 5 - $raRating;

													for( $i = 1; $i <= $raRating; $i++ ) {
														echo '<i class="fa fa-star" aria-hidden="true"></i> ';
													}

													if( fmod($raRating, 1) !== 0.00 ) {
														echo '<i class="fa fa-star-half-o" aria-hidden="true"></i> ';
													}

													for( $i = 1; $i <= $remaining; $i++ ) 
													{
														echo '<i class="fa fa-star-o" aria-hidden="true"></i>';
													}
												 ?>
											</div>
											<div class="date"><?php echo e(isset($review['reviewDate']) ? $review['reviewDate'] : ''); ?></div>
										</div>
										<div class="cs-name"><?php echo e(ucwords(isset($review['reviewer']) ? $review['reviewer'] : '')); ?></div>

										<div class="cs-info"><?php echo e(isset($review['reviewSummary']) ? $review['reviewSummary'] : ''); ?></div>
										<div class="cs-message"><?php echo isset($review['description']) ? $review['description'] : ''; ?></div>
									</div>
								</li>		
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>						
						</ul>
					</div>
				<?php endif; ?>
			 </div>
		</div>	
	</div>
</section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('afteElements'); ?>
	<div id="contactMe" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Send Me a Message</h4>
				</div>
				<div class="modal-body">
					<div class="default-form">
						

						<div class="form-group">
							<label>Message</label>
							<textarea rows="10" name="message" class="form-control"></textarea>
						</div>

						<div class="form-group text-right">
							<button class="btn btn-success send-message" data-agentid="<?php echo e($agent->id); ?>">Send</button>
							<button class="btn btn-danger" data-dismiss="modal">Cancel</button>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
	// verify csrf token
	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});

	$('.send-message').click(function(){

		$agentID = $(this).data('agentid');

		$data = {
			//'subject' : $('#contactMe input[name="title"]').val(),
			'message' : $('#contactMe textarea[name="message"]').val()
		}

		$.ajax({
			type: 'POST',
			url: baseUrl + '/customer/agent/' + $agentID + '/send-message',
			data: $data,
			success: function($response) {
				if( $response == 'success' ) 
				{
					swal('Success', 'Message Sent.', 'success');
					$('#contactMe').modal('hide');
				} else {
					swal('Error', 'Something went wrong', 'error');
				}
			}
		});

	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>