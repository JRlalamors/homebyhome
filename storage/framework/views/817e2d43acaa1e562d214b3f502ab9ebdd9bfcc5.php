<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;">
        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
            <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
            <td class="container1" style="margin: 0 auto!important;padding: 0px 0px 116px 0px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;background-image: url(https://www.homebyhome.com/images/emails/Congratulations.png);background-repeat: no-repeat;background-position: center bottom;display: block!important;max-width: 600px!important;clear: both!important;"><div class="content2" bgcolor="#ffffff" style="margin: 0 auto;padding: 0px 0px 0px 0px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
                    <table style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                            <td align="center" style="padding-top: 140px;margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                                <h3 style="font-family: 'Work Sans', sans-serif;font: ;font-weight: bold;padding: 10px 0px 0px 0px;margin: 0;line-height: 1.1;margin-bottom: 15px;color: #000;font-size: 40px;">Hello <?php echo e($customer->asUser->name); ?></h3>
                                <p class="lead" style="margin: 0;padding: 0px 20px;font-family: 'Work Sans', sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 24px;line-height: 27px;">Thank you for registering for Home By Home – a service helping home buyers and sellers save money while finding a qualified, experienced agent. Take some time to explore our site and brush up on your real estate FAQs. Meanwhile, we'll be busy finding you the right agents who will be contacting you shortly with a customized proposal just for you.</p>
                                <p class="lead" style="margin: 0;padding: 0px 20px;font-family: 'Work Sans', sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 24px;line-height: 27px;">In the meantime, if you have any questions, don’t hesitate to email us <a href="mailto:support@homebyhome.com">support@homebyhome.com</a> or call us at <a href="tel:8556008117">(855) 600-8117</a>.</p>
                                <br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                                Thanks again,<br style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                            </td>
                        </tr>
                    </table>
                </div></td>
            <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>