

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	customer
<?php $__env->stopSection(); ?>

<?php $__env->startSection('page-title'); ?>
	Search Customer
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>

<section class="customer-dashboard">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<?php echo Form::open(['route' => 'admin.customers.search-results', 'class' => 'default-form']); ?>

						
					<div class="row">
						<div class="col-xs-12 col-sm-6">							
							<div class="form-group">
								<label class="control-label" for="name">Name</label>
								<div class="control-form"><?php echo Form::text('name', null, ['class' => 'form-control', 'id' => 'name']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="zip">Zip Code</label>
								<div class="control-form"><?php echo Form::text('zip', null, ['class' => 'form-control', 'id' => 'zip']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="state">State</label>
								<div class="control-form"><?php echo Form::text('state', null, ['class' => 'form-control', 'id' => 'state']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="email">Email</label>
								<div class="control-form"><?php echo Form::email('email', null, ['class' => 'form-control', 'id' => 'email']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="phone">Phone #</label>
								<div class="control-form"><?php echo Form::text('phone', null, ['class' => 'form-control', 'id' => 'phone']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="agent_name">Agent Name</label>
								<div class="control-form"><?php echo Form::text('agent_name', null, ['class' => 'form-control', 'id' => 'agent_name
								']); ?></div>		
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="agent_email">Agent Email</label>
								<div class="control-form"><?php echo Form::email('agent_email', null, ['class' => 'form-control', 'id' => 'agent_email']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" for="agent_broker">Agent Broker</label>
								<div class="control-form"><?php echo Form::text('agent_broker', null, ['class' => 'form-control', 'id' => 'agent_broker']); ?></div>
							</div>			
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="form-group">
								<label class="control-label" >&nbsp;</label>
								<div class="checkbox-form">
									
									<label class="checkbox" for="buyer"><input id="buyer" type="radio" name="type"><span class="chk"></span><span class="text">Buyer</span></label>

								</div>
								<div class="checkbox-form">	
									<label class="checkbox" for="seller"><input id="seller" type="radio" name="type"><span class="chk"></span><span class="text">Seller</span></label>
									
								</div>
							</div>			
						</div>
						
						
						
						<div class="col-xs-12 col-sm-6">
							<div class="form-controls">
								<button class="btn btn-pink">Search</button>
							</div>	
						</div>
					
						
					</div>
				
				<?php echo Form::close(); ?>




			</div>
		</div>
	</div>
</section>
<section class="customer-menu">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">

				<div class="csmenu-wrapper">
					<div class="csmenu-list equal-items">
						<div class="item">
							<a href="<?php echo e(route('admin.customers.to-screen')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/customer/Customers-to-screen.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-agentsreview.svg')); ?>)"></div>	
									<h2 class="title">CUSTOMERS TO SCREEN</h2>
								</div>
							</a>
						</div><div class="item">
							
							<a href="<?php echo e(route('admin.customers.all')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/customer/See-all-customers.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-customerleads.svg')); ?>)"></div>
									<h2 class="title">SEE ALL CUSTOMER</h2>
								</div>
							</a>

						</div><div class="item">
							<a href="<?php echo e(route('admin.customers.without-agent')); ?>" class="item-wrapper">
								<div class="bg" style="background-image:url(<?php echo e(asset('images/admin/customer/Customers-wihtout-an-agent.jpg')); ?>)"></div>
								<div class="content">
									<div class="icon" style="background-image:url(<?php echo e(asset('images/icon-customerwoagent.svg')); ?>)"></div>
									<h2 class="title">CUSTOMERS WITHOUT AN AGENT</h2>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>	
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>