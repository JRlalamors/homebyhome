

<!-- Added Styles -->
<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
    page-basic
<?php $__env->stopSection(); ?>

<!-- Content -->
<?php $__env->startSection('content'); ?>
<section class="post-default">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1 class="hidden">Process</h1>

				<div class="process-template">
					<img src="../images/process-2.png" alt="" class="img-responsive">
					<img src="../images/process-3.png" alt="" class="img-responsive">
					<img src="../images/process-4.png" alt="" class="img-responsive">
					<img src="../images/process-5.png" alt="" class="img-responsive">
				</div>



				<!-- <h1>Omnes enim iucundum motum, quo sensus hilaretur.</h1>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quasi ego id curem, quid ille aiat aut neget. Quid est, quod ab ea absolvi et perfici debeat? Quo tandem modo? Duo Reges: constructio interrete. <b>Erat enim Polemonis.</b> Nam aliquando posse recte fieri dicunt nulla expectata nec quaesita voluptate. Nam si beatus umquam fuisset, beatam vitam usque ad illum a Cyro extructum rogum pertulisset. Isto modo, ne si avia quidem eius nata non esset. </p>

				<p>Quid igitur, inquit, eos responsuros putas? Duo enim genera quae erant, fecit tria. <a href='http://loripsum.net/' target='_blank'>Itaque ad tempus ad Pisonem omnes.</a> Est enim effectrix multarum et magnarum voluptatum. Quasi ego id curem, quid ille aiat aut neget. Bonum incolumis acies: misera caecitas. Quid ad utilitatem tantae pecuniae? Idcirco enim non desideraret, quia, quod dolore caret, id in voluptate est. </p>

				<pre>
				Itaque omnis honos, omnis admiratio, omne studium ad
				virtutem et ad eas actiones, quae virtuti sunt consentaneae,
				refertur, eaque omnia, quae aut ita in animis sunt aut ita
				geruntur, uno nomine honesta dicuntur.

				Dicet pro me ipsa virtus nec dubitabit isti vestro beato M.
				</pre>


				<dl>
					<dt><dfn>Sed fortuna fortis;</dfn></dt>
					<dd>Nec vero sum nescius esse utilitatem in historia, non modo voluptatem.</dd>
					<dt><dfn>Numquam facies.</dfn></dt>
					<dd>Tu vero, inquam, ducas licet, si sequetur;</dd>
				</dl>


				<blockquote cite='http://loripsum.net'>
					Illi igitur antiqui non tam acute optabiliorem illam vitam putant, praestantiorem, beatiorem, Stoici autem tantum modo praeponendam in seligendo, non quo beatior ea vita sit, sed quod ad naturam accommodatior.
				</blockquote>


				<p>Sin dicit obscurari quaedam nec apparere, quia valde parva sint, nos quoque concedimus; Mihi, inquam, qui te id ipsum rogavi? Quodsi ipsam honestatem undique pertectam atque absolutam. <b>Quibusnam praeteritis?</b> Color egregius, integra valitudo, summa gratia, vita denique conferta voluptatum omnium varietate. Ubi ut eam caperet aut quando? </p>

				<p><a href='http://loripsum.net/' target='_blank'>Cur id non ita fit?</a> <i>Occultum facinus esse potuerit, gaudebit;</i> Ut enim consuetudo loquitur, id solum dicitur honestum, quod est populari fama gloriosum. Tollenda est atque extrahenda radicitus. Omnes enim iucundum motum, quo sensus hilaretur. Fortasse id optimum, sed ubi illud: Plus semper voluptatis? <mark>Occultum facinus esse potuerit, gaudebit;</mark> <a href='http://loripsum.net/' target='_blank'>Multoque hoc melius nos veriusque quam Stoici.</a> Ego quoque, inquit, didicerim libentius si quid attuleris, quam te reprehenderim. Quis non odit sordidos, vanos, leves, futtiles? </p>

				<ul>
					<li>Ab his oratores, ab his imperatores ac rerum publicarum principes extiterunt.</li>
					<li>Ab hoc autem quaedam non melius quam veteres, quaedam omnino relicta.</li>
					<li>Suo genere perveniant ad extremum;</li>
				</ul>


				<ol>
					<li>Illis videtur, qui illud non dubitant bonum dicere -;</li>
					<li>Apparet statim, quae sint officia, quae actiones.</li>
					<li>Animum autem reliquis rebus ita perfecit, ut corpus;</li>
					<li>Nihilne te delectat umquam -video, quicum loquar-, te igitur, Torquate, ipsum per se nihil delectat?</li>
				</ol>


				<p>An nisi populari fama? Sed vos squalidius, illorum vides quam niteat oratio. Que Manilium, ab iisque M. Quid censes in Latino fore? Dat enim intervalla et relaxat. Videamus igitur sententias eorum, tum ad verba redeamus. </p> -->




			</div>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>


<!-- Added Scripts -->
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.process', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>