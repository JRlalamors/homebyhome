<nav class="page-nav">
<!-- Menu if Customer -->



    <ul class="list-inline pull-right">
        
        <?php if( $authUser ): ?>  

            <li><a href="<?php echo e($authUser->isCustomer() ? route('customer.account') : route('agent.account')); ?>">My Proposals</a></li>
            <li><a href="<?php echo e($authUser->isCustomer() ? route('customer.profile') : route('agent.profile')); ?>">My Profile</a></li>   

        <?php else: ?>
            <li><a href="<?php echo e($authUser && $authUser->isCustomer() ? route('customer.list.add') : route('register')); ?>">Find an Agent</a></li>
            <li>
                <a href="javascript:;">About Us</a>
                <ul class="dropdown-menu">
                    <li><a href="<?php echo e(route('about')); ?>">Our Story</a></li>
                    <li><a href="<?php echo e(route('sellers.process')); ?>">Our Process</a></li>
                   
                    <li><a href="<?php echo e(route('contact')); ?>">Contact Us</a></li>
                </ul>   
            </li>


        <?php endif; ?>

        <li><a href="javascript:;">Resources</a>

            <ul class="dropdown-menu">
                <li><a href="https://www.homebyhome.com/blog/first-time-home-buyers-guide-things-know/">Buyer's Guide</a></li>
                <li><a href="https://www.homebyhome.com/blog/home-sellers-guide/">Seller's Guide</a></li>
                <li><a href="https://www.homebyhome.com/blog/">Blog</a></li>
            </ul>   
        </li>

        <?php if( !$authUser ): ?>
            <li>
                <a href="<?php echo e(route('login')); ?>">Login</a>
            </li>
        <?php endif; ?>

        <?php if( $authUser ): ?>      
           
            <li class="dropdown">
                <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown">   
                    <?php 
                        if( $authUser->isAgent() ) 
                        {
                            if( $authUser->profile_photo )
                            {                                
                                $photo = $authUser->profile_photo;
                            } else {

                                if( isset($authUser->asAgent->zillowApi()['proInfo']['photo']) )
                                {
                                    $photo = $authUser->asAgent->zillowApi()['proInfo']['photo'];
                                } else {
                                    $photo = asset('images/user-blank.svg');
                                }

                            }

                        } else {

                            if($authUser->profile_photo)
                            {
                                $photo = $authUser->profile_photo;
                            } else {
                                $photo = asset('images/user-blank.svg');
                            }

                        }
                     ?>
                    <div class="user-avatar"><img class="img-responsive" src="<?php echo e($photo); ?>"></div>             
                    
                    <div class="user-name"><span class="name">Hi <?php echo e($authUser->first_name); ?></span><span class="arrow"><i class="fa fa-angle-down" aria-hidden="true"></i></span></div>
                </a>
                <ul class="dropdown-menu">
                   
                    <li><a href="<?php echo e(url('logout')); ?>">Logout</a></li>
                </ul>
            </li>
        <?php else: ?> 
           <li><a href="<?php echo e(route('agent')); ?>" class="btn btn-info btn-circle blue">Are you an Agent?</a></li>
        <?php endif; ?>
    </ul>
</nav>