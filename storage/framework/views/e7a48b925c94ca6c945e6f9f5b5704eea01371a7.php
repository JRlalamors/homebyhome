<?php $__env->startComponent('mail::message'); ?>
    <table class="body-wrap" style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;width: 100%;">
        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
            <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
            <td class="container1" style="margin: 0 auto!important;padding: 0px 0px 116px 0px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;background-image: url(https://www.homebyhome.com/images/emails/Congratulations.png);background-repeat: no-repeat;background-position: center bottom;display: block!important;max-width: 600px!important;clear: both!important;"><div class="content2" bgcolor="#ffffff" style="margin: 0 auto;padding: 0px 0px 0px 0px;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;max-width: 600px;display: block;">
                    <table style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                        <tr style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                            <td style="padding-top: 140px;margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;">
                                <p class="lead" style="margin: 0;padding: 0px 20px;font-family: 'Work Sans', sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 24px;line-height: 27px;"><label>Name: </label> <?php echo e($customer->asUser->name); ?></p>
                                <p class="lead" style="margin: 0;padding: 0px 20px;font-family: 'Work Sans', sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 24px;line-height: 27px;"><label>Email: </label> <?php echo e($customer->asUser->email); ?></p>
                                <p class="lead" style="margin: 0;padding: 0px 20px;font-family: 'Work Sans', sans-serif;margin-bottom: 10px;font-weight: normal;font-size: 24px;line-height: 27px;"><label>Phone #: </label> <?php echo e($customer->asUser->phone_num); ?>

                            </td>
                        </tr>
                    </table>
                </div></td>
            <td style="margin: 0;padding: 0;font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif;"></td>
        </tr>
    </table>
<?php echo $__env->renderComponent(); ?>