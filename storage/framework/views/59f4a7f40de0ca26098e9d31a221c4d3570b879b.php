

<?php $__env->startSection('styles'); ?>
	<style type="text/css">
		.comments-section {
			max-width: 100%;
		}

		.comments-section .cs-list {
			padding: 0;
		}

		.chat-wrapper {
			position: fixed;
			bottom: 10px;
			border: 1px solid #0C4672;
			background: #fff;
			padding: 10px;
			right: 10px;
			z-index: 10;
			display: none;
		}

		.chat-header {
			margin-top: -10px;
			margin-left: -10px;
			margin-right: -10px;
			padding: 10px;
			background: #0C4672;
		}

		.chat-name {
			margin: 0;
			float: left;
			color: #fff;
		}

		.chat-body {
			height: 240px;
			border-top: 1px solid #ccc;
			border-bottom: 1px solid #ccc;
			margin-left: -10px;
			margin-right: -10px;
			margin: 5px 0;
		}

		.chat-close {
			float: right;
			cursor: pointer;
			color: #fff;
			font-size: 16px;
		}

		.chat-close:hover {
			text-decoration: none;
			color: #fff;
		}

		.chat-wrapper .btn {
			padding: 10px;
			font-size: 14px;
			font-size: 16px;
		}

	</style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('body-class'); ?>
	agents-dashboard pagetitle-off
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
	<div class="default-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">

					<section class="agent-notice" >

						<a class="ctrl" href="javascript:;">Close Notice <i class="fa fa-times" aria-hidden="true"></i></a>
						<div class="bg" style="background-image:url(<?php echo e(asset('images/bg-agent-notice.jpg')); ?>)"></div>

						<div class="contents">
							<h2>Thank you for registering with HomebyHome.<br/>Please email or call us (<a href="tel:8556008117">855-600-8117</a>) <br/>with any questions.</h2>
						</div>

						<div class="contents collapsed">
							<h2>Email or call us <a href="tel:8556008117">(855) 600-8117</a></h2>
						</div>

					</section>

					<?php echo $__env->make('partials.form-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

					<div class="visible-xs">
						<div class="tab-mobile-controls ">
							<select class="form-control">
								<option value="#tab-requests">Request for Proposals</option>
								<option value="#tab-outstandings">Outstanding Proposals</option>
								<option value="#tab-customers">Customers</option>
								<option value="#tab-zillow">Account Settings</option>
								<option value="#tab-qa">Q+A</option>
							</select>
						</div>
					</div>
					<div class="tab-controls hidden-xs">
						<ul class="nav nav-tabs nav-justified">
							<li class="active"><a data-toggle="tab" class="theme-pink" href="#tab-requests">Request for Proposals</a></li>
							<li><a class="theme-pink" data-toggle="tab" href="#tab-outstandings">Outstanding Proposals</a></li>
							<li><a class="theme-pink" data-toggle="tab" href="#tab-customers">Customers</a></li>
							<li><a class="theme-pink" data-toggle="tab" href="#tab-zillow">Account Settings</a></li>
							<li><a class="theme-pink" data-toggle="tab" href="#tab-qa">Q+A</a></li>
						</ul>
					</div>

					<div class="tab-content">
						<div id="tab-requests" class="tab-pane fade in active">
							<div class="table-responsive default-table">
								<table class="table">
									<thead>
									<tr>
										<th class="text-center">Date Requested</th>
										<th class="text-center">Client's Name</th>
										<th class="text-center">Type</th>
										<th class="text-center">Details</th>
										<th class="text-center">Proposal</th>
									</tr>
									</thead>
									<tbody>
									<?php $__empty_1 = true; $__currentLoopData = $deals; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
										<tr>
											<td class="text-center"><?php echo date('M d, o', strtotime($deal->pivot->created_at)); ?></td>
											<td class="text-center"><?php echo e($deal->customer->asUser->name); ?></td>
											<td class="text-center"><?php echo e($deal->type()); ?></td>
											<td class="text-center"><a href="<?php echo e(route('agent.list.customer-details', $deal)); ?>" class="blue">View Details</a></td>
											<td class="text-center"><a href="<?php echo e(route('agent.list.details', $deal)); ?>" class="view-list" data-id="<?php echo e($deal->id); ?>"><?php echo e($deal->agentHasProposal($authUser->asAgent->id) ? 'View Proposal' : 'Submit'); ?></a></td>
											
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
										<tr><td colspan="4">Deals not found..</td></tr>
									<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>

						<div id="tab-zillow" class="tab-pane fade">
							<div class="default-content">
								<?php echo Form::open(['route' => 'agent.account.set-zillow-name', 'class' => 'default-form']); ?>

								<div class="form-group">
									<label>Zillow Name:</label>
									<?php echo Form::text('zillow_name', $authUser->asAgent->getOtherData('admin_zillow_url') ? $authUser->asAgent->getOtherData('admin_zillow_url') : $authUser->asAgent->zillow_url, ['class' => 'form-control', 'placeholder' => 'https://www.zillow.com/profile/ZillowAccountName/']); ?> <br>

									<label>LinkedIn:</label>
									<?php echo Form::text('linkedin_url', $authUser->asAgent->getOtherData('admin_linkedin_url') ? $authUser->asAgent->getOtherData('admin_linkedin_url') : $authUser->asAgent->linkedin_url, ['class' => 'form-control', 'placeholder' => 'Profile url']); ?> <br>

									<label>Website Url:</label>
									<?php echo Form::text('website_url', $authUser->asAgent->getOtherData('admin_website_url') ? $authUser->asAgent->getOtherData('admin_website_url') : $authUser->asAgent->website_url, ['class' => 'form-control', 'placeholder' => 'http://www.samplewebsite.com']); ?>

								</div>
								<hr>
								<div class="form-group">
									<label>Password</label>
									<?php echo Form::password('new_password', ['class' => 'form-control', 'placeholder' => 'New password']); ?>

								</div>
								<div class="form-group">
									<?php echo Form::password('confirm_new_password', ['class' => 'form-control', 'placeholder' => 'Confirm new password']); ?>

								</div>
								<div class="form-group">
									<button class="btn btn-success">Update</button>
								</div>
								<?php echo Form::close(); ?>

							</div>
						</div>

						<div id="tab-outstandings" class="tab-pane fade">
							<div class="table-responsive default-table">
								<table class="table">
									<thead>
									<tr>
										<th class="text-center">Date Requested</th>
										<th class="text-center">Client's Name</th>
										<th class="text-center">Type</th>
										<th class="text-center">Details</th>
									</tr>
									</thead>
									<tbody>
									<?php $__empty_1 = true; $__currentLoopData = $OPs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $deal): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
										<tr>
											<td class="text-center"><?php echo date('M d, o', strtotime($deal->agents->first()->pivot->created_at)); ?></td>
											<td class="text-center"><?php echo e($deal->customer->asUser->name); ?></td>
											<td class="text-center"><?php echo e($deal->type()); ?></td>
											<td class="text-center"><a href="<?php echo e(route('agent.list.customer-details', $deal)); ?>" class="blue">View Details</a></td>
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
										<tr><td colspan="4">Deals not found..</td></tr>
									<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>

						<div id="tab-customers" class="tab-pane fade">
							<div class="table-responsive default-table">
								<table class="table">
									<thead>
									<tr>
										<th class="text-center">Name</th>
										<th class="text-center">Email</th>
										<th class="text-center"></th>
										<th class="text-center">View Profile</th>
									</tr>
									</thead>
									<tbody>
									<?php $__empty_1 = true; $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $customer): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
										<tr>
											<td class="text-center"><?php echo e($customer->asUser->name); ?></td>
											<td class="text-center"><?php echo e($customer->asUser->email); ?></td>
											<td class="text-center"></td>
											<td class="text-center"><a href="<?php echo e(route('agent.customer.profile', $customer)); ?>" class="blue">View Profile</a></td>
										</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
										<tr><td colspan="10">Customers not found..</td></tr>
									<?php endif; ?>
									</tbody>
								</table>
							</div>
						</div>

						<div id="tab-qa" class="tab-pane fade">
							<div class="default-content">
								<?php echo Form::model($authUser->asAgent, ['route' => ['agent.account.settings.update'],  'class' => 'default-form form-qa']); ?>

								<div class="form-group">
									<label class="control-label" for="question_1">Why should you hire me?</label>
									<?php echo Form::textarea('question_1', $authUser->asAgent->getOtherData('question_1'), ['class' => 'form-control', 'id' => 'question_1']); ?>

								</div>

								<div class="form-group">
									<label class="control-label" for="question_2">What do i do better than most agents?</label>
									<?php echo Form::textarea('question_2', $authUser->asAgent->getOtherData('question_2'), ['class' => 'form-control', 'id' => 'question_2']); ?>

								</div>

								<div class="form-controls text-right">
									<button class="btn btn-primary">Submit</button>
								</div>
								<?php echo Form::close(); ?>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('afteElements'); ?>
	<div id="sendProposal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Details</h4>
				</div>

				<div class="modal-body">
					<div class="list-details">

					</div>
					<hr>
					<?php echo $__env->make('partials.form-ajax-errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('partials.form-ajax-success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="proposal">

					</div>
				</div>

				<?php echo $__env->make('layouts.partials.form-loader', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
			</div>

		</div>
	</div>

	<div class="chat-wrapper">
		<div class="chat-header">
			<p class="chat-name">Agent Name</p>
			<a class="chat-close">&times;</a>
			<div class="clearfix"></div>
		</div>
		<div class="chat-body">

		</div>
		<div class="chat-actions">
			<input type="text" name="chat-messsage" class="chat-message" placeholder="Type something..">
			<button class="btn btn-xs btn-blue  ">Send</button>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
	<script>
        $(document).ready(function(){


            $('.tab-mobile-controls').on('change','select',function(){
                console.log($(this).val());

                //tab-controls

                $('.tab-controls a[href*="'+$(this).val()+'"]').click();
            });

            $('.tab-controls').on('click','a',function(){
                console.log($(this).attr('href'));

                $('.tab-mobile-controls select').val($(this).attr('href'));

            });

            var $baseUrl = $('meta[name="base-url"]').attr('content');

            // verify csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.agent-notice').on('click','.ctrl',function(){
                var $wrapper = $(this).closest('.agent-notice');

                $wrapper.toggleClass('active');

                if( !$wrapper.hasClass('active') )
                {
                    $(this).html('Close Notice <i class="fa fa-times" aria-hidden="true"></i>');
                } else {
                    $(this).html('View Notice <i class="fa fa-sticky-note-o" aria-hidden="true"></i>');
                }

            });

            // submit proposal
            var $listID;
            $('.view-list').click(function(){
                $listID = $(this).data('id');
            });

            //updateRebateInput($('[name="rebate_type"]').val());
            $('body').on('change', '[name="rebate_type"]', function(){
                $type = $(this).val();

                updateRebateInput($type);
            });

            function updateRebateInput($type) {
                if ($type == 'fixed') {
                    $('[name="rebate"]').removeAttr('max');
                    $('.dollar').removeClass('hidden');
                    $('.percentage').addClass('hidden');
                } else {
                    $('[name="rebate"]').attr('max', 99);
                    $('.percentage').removeClass('hidden');
                    $('.dollar').addClass('hidden');
                }
            }

            $('#sendProposal').on('show.bs.modal', function() {

                $.ajax({
                    type: 'POST',
                    url: $baseUrl + '/agent/account/list/' + $listID + '/details',
                    beforeSend: function() {
                        $('.form-loader-wrap').show();
                    },
                    success: function($list) {
                        $('#sendProposal')
                            .find('.list-details')
                            .html( $list.listDetail )
                            .end()
                            .find('.proposal')
                            .html( $list.toReply );
                        updateRebateInput($('[name="rebate_type"]').val());

                        $('.form-loader-wrap').fadeOut();
                    }
                });

            });

            $(document).on('click', '.submit-proposal', function() {

                var $datas = {
                    'rebate_type' : $('.rebate-type').val(),
                    'rebate' : $('.rebate').val(),
                    'propose_1' : $('.propose-1').text(),
                    'propose_2' : $('.propose-2').text()
                };

                $.ajax({
                    type: 'POST',
                    url: $baseUrl + '/agent/account/list/' + $listID + '/submit-proposal',
                    data: $datas,
                    beforeSend: function() {
                        $('.form-loader-wrap').show();
                    },
                    success: function($response) {

                        $('.form-loader-wrap').fadeOut();

                        if( $response.errors )
                        {
                            $('.form-ajax-notif .alert-content ul').html("");

                            for( $i in $response.errors )
                            {
                                $('.form-ajax-notif')
                                    .find('.alert-content ul')
                                    .append('<li>'+ $response.errors[$i] +'</li>')
                                    .end()
                                    .show();
                            }

                        } else {
                            $('.alert.alert-success')
                                .find('.alert-content ul')
                                .html("")
                                .append('<li>' + $response.success + '</li>')
                                .end()
                                .removeClass('hidden');

                            setTimeout(function(){
                                window.location = $response.route;
                            }, 2000);
                        }

                    }
                })

            });

        });
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.default', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>