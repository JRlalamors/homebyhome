<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->increments('id');   
            $table->integer('customer_id')->nullable();
            $table->integer('agent_id')->nullable();
            $table->enum('list_type', ['buyer', 'seller']);
            $table->integer('property_type_id');
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip_postal');
            $table->bigInteger('sq_feet_min')->nullable();
            $table->bigInteger('sq_feet_max')->nullable();
            $table->enum('bedrooms', ['1', '2', '3', '4', '5', '6', '7', '8'])->nullable();
            $table->enum('bathrooms', ['1', '1.5', '2', '2.5', '3', '3.5'])->nullable();
            $table->string('until')->nullable();
            $table->string('text_until')->nullable();
            $table->string('budget')->nullable();
            $table->string('text_budget')->nullable();
            $table->text('other_data')->nullable();
            $table->boolean('closed_deal')->nullable();
            $table->date('closed_at')->nullable();
            $table->softDeletes();
            $table->timeStamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('listings');
    }
}
