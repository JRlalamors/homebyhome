<?php

namespace App\Model;

use App\User;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Proposal;
use Illuminate\Database\Eloquent\Model;

class Customer extends BaseModel
{
    
	protected $fillable = [
		'status_to_lender',
	];

	protected $rules = [
		//'status_to_lender' => 'required'
	];

	protected $casts = [
        'other_data' => 'array',
    ];

	/*
	* Relations
	**/
	public function asUser() 
	{
		return $this->belongsTo(User::class, 'user_id');
	}

	public function listings() 
	{
		return $this->hasMany(Listing::class);
	}

	public function proposals()
	{
		return $this->hasMany(Proposal::class);
	}

	/*
	* Getter Method
	**/
	public function getLenderStatusAttribute()
	{
		return ucfirst( $this->status_to_lender );
	}

	/*
	* Helper Methods
	**/
	public function sellerList() 
	{
		return $this->listings->filter(function($list){
			return $list->list_type == 'seller';
		});
	}

	public function buyerList() 
	{
		return $this->listings->filter(function($list) {
			return $list->list_type == 'buyer';
		});
	}
}
