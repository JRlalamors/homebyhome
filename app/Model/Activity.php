<?php

namespace App\Model;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Activity extends Model
{
    
	protected $table = 'activities';

	protected $rules = [
		'user_id' => 'required',
		'action_taken' => 'required',
		'date' => 'required'
	];

	/*
	* Helper Method
	**/

	public function addActivity($userID, $action, $date = null) 
	{
	    $ip = Request::ip();

		$this->user_id = $userID;
		$this->action_taken = $action;
		$this->date = $date ? $date : Carbon::now();
		$this->ip = $ip;

		$this->save();

	}

}
