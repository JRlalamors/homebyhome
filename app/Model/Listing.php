<?php

namespace App\Model;

use App\Model\Agent;
use App\Model\Customer;
use App\Model\Proposal;
use App\Model\PropertyType;
use Illuminate\Database\Eloquent\Model;

class Listing extends BaseModel
{

    const BUYER = 'buyer';
    const SELLER = 'seller';

    protected $fillable = [
    	'list_type',
    	'property_type_id',
    	'zip_postal',
    	'sq_feet_min',
    	'sq_feet_max',
    	'bedrooms',
    	'bathrooms',
    	'until'
    ];

    public $rules = [];

    public function customer() 
    {
        return $this->belongsTo(Customer::class);
    }

    public function agents() 
    {
        return $this->belongsToMany(Agent::class)->withTimeStamps();
    }

    public function selectedAgent()
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function property() 
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    public function proposals() 
    {
        return $this->hasMany(Proposal::class);
    }


    /*
    * Helper Methods
    **/
    public function selectedProposal() 
    {
        $agentID = $this->selectedAgent ? $this->selectedAgent->id : null;
        
        if( $agentID == null )
        {
            return null;
        }

        $proposal = Proposal::where('listing_id', $this->id)
                                ->where('agent_id', $agentID)
                                ->first();

        return $proposal;
    }

    public function streetAddress() {
        $other = $this->other_data != null ? json_decode( $this->other_data ) : null;
        
        if( $other != null && isset($other->street_address) ) 
        {
           return $other->street_address;  
        }
         
        return false;
    }

    public function unitNo() {
        $other = $this->other_data != null ? json_decode( $this->other_data ) : null;
        
        if( $other != null && isset( $other->unit_no ) ) 
        {
           return $other->unit_no;  
        }

        return false;
    }

    public function getCompleteAddressAttribute()
    {
        if ($this->list_type == self::SELLER) {
            $address = $this->unitNo() != '' ? $this->unitNo() : '';
            $address .= $this->streetAddress() != '' ? $this->streetAddress() . ", " : '';
            $address .= $this->state != '' ? $this->state . ", " : '';
            $address .= $this->city != '' ? $this->city : '';

            return $address;
        } else {
            return $this->zip_postal;
        }
    }

    public function getSquareFeetAttribute()
    {

        if( $this->sq_feet_min == null )
        {
            return $this->sq_feet_max;
        }

        return $this->sq_feet_min . ' - ' . $this->sq_feet_max;
    }

    public function type() 
    {
        return $this->list_type == 'seller' ? 'Seller' : 'Buyer';
    }

    public function isBuyer()
    {
        return $this->list_type == 'buyer' ?: false;
    }

    public function isSeller()
    {
        return $this->list_type == 'seller' ?: false;
    }


    public function agentHasProposal($agentID)
    {
        $proposal = Proposal::where('agent_id', $agentID)
                              ->where('listing_id', $this->id)
                              ->first();

        if( $proposal ) 
        {
            return true;
        }

        return false;
    }

    public function hasSelectedAProposal() 
    {
        if( $this->agent_id == null) 
        {
            return false;
        } 

        return true;
    }

    public function proposal($agentID) 
    {

        $proposal = Proposal::where('agent_id', $agentID)
                              ->where('listing_id', $this->id)
                              ->first();

        return $proposal;

    }
}
