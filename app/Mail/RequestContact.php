<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestContact extends Mailable
{
    use Queueable, SerializesModels;
	
	public $firstname;
	public $lastname;
	public $email;
	public $company;
	public $phone;
	public $comment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstname, $lastname, $email, $company, $phone, $comment)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->company = $company;
        $this->phone = $phone;
        $this->comment = $comment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->subject('Contact Us')
                    ->markdown('email.request-contact')
                    ->with([
                        'firstname' => $this->firstname,
                        'lastname' => $this->lastname,
                        'email' => $this->email,
                        'company' => $this->company,
                        'phone' => $this->phone,
                        'comment' => $this->comment
                    ]);
    }
}
