<?php

namespace App\Mail;

use App\Model\Agent;
use App\Model\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AgentHired extends Mailable
{
    use Queueable, SerializesModels;

    public $agent;
    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Agent $agent, Customer $customer)
    {
        $this->agent = $agent;
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Congratulations! You've been hired by " . $this->customer->asUser->name)
                    ->markdown('email.agent-hired')
                    ->with([
                        'agent' => $this->agent,
                        'customer' => $this->customer
                    ]);
    }
}
