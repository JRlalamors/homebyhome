<?php

namespace App\Mail;

use App\Model\Customer;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerSignupMailUser extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Welcome to HomebyHome')
                    ->markdown('email.customer-signup-user')
                    ->with([
                        'customer' => $this->customer,
                        'email_verification' => url('verify-email/') . bcrypt($this->customer->asUser->email)
                    ]);
    }
}
