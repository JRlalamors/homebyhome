<?php

namespace App\Mail;

use App\Model\Agent;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerifiedAgent extends Mailable
{
    use Queueable, SerializesModels;

    public $agent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your profile has been approved.')
                    ->markdown('email.verified-agent')
                    ->with([
                        'agent' => $this->agent,
                        'password' => $this->agent->getOtherData('plain_password')
                    ]);
    }
}
