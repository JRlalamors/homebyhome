<?php

namespace App\Mail;

use App\Model\Agent;
use App\Model\Listing;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SelectedAgentMailCustomer extends Mailable
{
    use Queueable, SerializesModels;

    public $listing;
    public $agent;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Listing $listing, Agent $agent)
    {
        $this->listing = $listing;
        $this->agent = $agent;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->subject('Agent Selected')
            ->markdown('email.agent-selected-mail-customer')
            ->with([
                'listing' => $this->listing,
                'agent' => $this->agent,
                'customer' => $this->listing->customer
            ]);
    }
}
