<?php

namespace App\Http\Controllers\User;

use Illuminate\Container\Container;
use Illuminate\Mail\Markdown;
use Illuminate\Support\Facades\Auth;
use ImageResize;
use Carbon\Carbon;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use App\Mail\AgentHired;
use App\Mail\SendMessage;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Mail\CustomerHasSelected;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class CustomerController extends Controller
{

	public function account(Request $request) 
	{
		$list = new Listing;

		$sellingList = $list->search(['proposals' => function($proposal) {
            $proposal->with('agent.asUser');
        }, 'property'], 6, ['customer_id' => $request->user()->asCustomer->id, 'list_type' => 'seller']);

		$buyingList = $list->search(['proposals' => function($proposal) {
            $proposal->with('agent.asUser');
        }, 'property'], 6, ['customer_id' => $request->user()->asCustomer->id, 'list_type' => 'buyer']);

        $lastLogin = with(new Carbon($request->user()->last_login));
        $createdAt = with(new Carbon($request->user()->created_at));
        $now = Carbon::now();

        if( $createdAt->diffInMinutes($lastLogin) <= 5 && $now->diffInMinutes($lastLogin) <= 5 )
        {
            $newUser = true;
        } else {
            $newUser = false;
        }

	   return view('user.customer.index', compact('sellingList', 'buyingList', 'newUser'));
	}

	public function profile(Request $request) 
	{

        $lists = $request->user()
                         ->asCustomer
                         ->listings()
                            ->with('property')
                         ->orderByDesc('created_at')
                         ->get()
                         ->values();

		return view('user.customer.profile', compact('lists'));
	}

	public function agentOverview(Customer $customer, Agent $agent) 
	{

        $zillowApi = $agent->zillowApi();

        $agent = $agent->asUser;

		return view('user.customer.agent-overview', compact('agent', 'zillowApi'));
	}

	public function agentProposal(Listing $list, Agent $agent, Request $request) 
	{

        $proposal = $list->proposal($agent->id);

        $zillowApi = $agent->zillowApi();

        $customer =  $request->user()->name;

		return view('user.customer.agent-proposal', compact('agent', 'proposal', 'zillowApi', 'list', 'customer'));
	}

	public function hireAgent(Listing $list, Agent $agent, Request $request) 
	{
        $list->agent_id = $agent->id;
        $list->save();

        // send mail to hired agent
        Mail::to($agent->asUser->email)->send(new AgentHired($agent, $list->customer));

        // send mail to agents who haven't been selected
        foreach( $list->agents as $unSelectedAgent )
        {
            if( $unSelectedAgent->id != $agent->id )
            {
                Mail::to($unSelectedAgent->asUser->email)->send(new CustomerHasSelected($list, $unSelectedAgent, $request->user()->asCustomer));
            }
        }

        return redirect()->route('customer.account')->withSuccess('Agent selected for proposal');
	}

	public function addList() 
	{
		return view('user.customer.add-list');
	}

    public function validateForm(Request $request)
    {
        $list = new Listing;

        // validating list               
        $list->fill($request->all());
        $list->until = with(new Carbon($request->until))->format('o-m-d');
        $list->closed_deal = 0;

        $errorMessages = [
            'sq_feet_max.greater_than_field' => 'The square feet max field must be higher than square feet min field.'
        ];

        $reqArray = [];
        $validateArr = [];

        if( array_key_exists('zip_postal', $request->all()) || $request->has('zip_postal') ) {
            $reqArray['zip_postal'] = $request->zip_postal;
            $validateArr['zip_postal'] = 'required|numeric|min:5';
        }

        if( array_key_exists('budget', $request->all()) || $request->has('budget') ) {
            $reqArray['budget'] = (int) preg_replace(array('/\,/', '/\$/'), array(''), $request->budget);
            $validateArr['budget'] = 'required'; //|numeric
            $reqArray['text_budget'] = $request->budget; //(int) preg_replace(array('/\,/', '/\$/'), array(''), $request->budget)
            $validateArr['text_budget'] = 'required'; //|numeric
        }

        if( (array_key_exists('sq_feet_min', $request->all()) || $request->has('sq_feet_min')) && (array_key_exists('sq_feet_max', $request->all()) || $request->has('sq_feet_max')) ) {
            $reqArray['sq_feet_min'] = $request->sq_feet_min;
            $reqArray['sq_feet_max'] = $request->sq_feet_max;

            if( $request->list_type == 'seller' )            
            {
                $validateArr['sq_feet_max'] = 'required|numeric';
            } else {            
                $validateArr['sq_feet_min'] = 'required_with:sq_feet_max|numeric';
                $validateArr['sq_feet_max'] = 'required_with:sq_feet_min|numeric|greater_than_field:sq_feet_min';
            }
        }

        if( array_key_exists('bedrooms', $request->all()) || $request->has('bedrooms') ) {
            $reqArray['bedrooms'] = $request->bedrooms;
            $validateArr['bedrooms'] = 'required';
        }

        if( array_key_exists('bathrooms', $request->all()) || $request->has('bathrooms') ) {
            $reqArray['bathrooms'] = $request->bathrooms;
            $validateArr['bathrooms'] = 'required';
        }

        if( array_key_exists('until', $request->all()) || $request->has('until') ) {
            /*$reqArray['until'] = $request->until;
            $validateArr['until'] = 'required'; // |date*/
            $reqArray['text_until'] = $request->until;
            $validateArr['text_until'] = 'required'; //|date
        }

        $validateList = Validator::make($reqArray, $validateArr, $errorMessages);

        $validateList->fails();

        $errors = $validateList->errors();

        if( count( $errors ) > 0 ) {
            return response( $errors );
        }

        return response('validated');
    }

	public function storeList(Request $request)
	{
		$list = new Listing;

		$list->fill($request->all());

        /* Depreciated
        $today = Carbon::now();

        switch( $request->until )
        {
            case 'this-month' :
                $until = $today->endOfMonth()->format('o-m-d');
                break;

            case 'next-month' :
                $until = $today->addDays(45)->format('o-m-d');
                break;

            case 'two-weeks' :
                $until = $today->addWeeks(2)->format('o-m-d');

            default : 
                $until = $today->format('o-m-d');
        }*/

        //$list->until = $until;
        $list->text_until = $request->until;
        $list->budget = (int) preg_replace(array('/\,/', '/\$/'), array(''), $request->numeric_budget);
        $list->text_budget = $request->list_type == 'seller' ? $request->budget : '$' . number_format($request->budget, 0);
        $list->closed_deal = 0;
        $list->customer_id = $request->user()->asCustomer->id;

		$errorMessages = [
            'sq_feet_max.greater_than_field' => 'The square feet max field must be higher than square feet min field.'
        ];

        // re-eval requests
        $req = [];
        foreach( $request->all() as $key => $r )
        {
            $req[$key] = $r;
        }
        $req['budget'] = (int) preg_replace(array('/\,/', '/\$/'), array(''), $request->budget);

		if( $request->list_type == 'seller' ) {         
            $list->city = $request->city;
            $list->state = $request->state;
            $list->other_data = json_encode(array('street_address' => $request->street_address, 'unit_no' => $request->unit_no));

            // validating rules
            $validateList = Validator::make($req, [
                'list_type' => 'required',
                'property_type_id' => 'required',
                'zip_postal' => 'required',
                //'city' => 'required',
                //'state' => 'required',
                //'sq_feet_max' => 'required|numeric',
                //'bedrooms' => 'required',
                //'bathrooms' => 'required',
                //'until' => 'required',
                'budget' => 'required' // |numeric
            ], $errorMessages);
        } else {

            // validating rules
            $validateList = Validator::make($req, [
                'list_type' => 'required',
                'property_type_id' => 'required',
                'zip_postal' => 'required',
                //'sq_feet_min' => 'required_with:sq_feet_max|numeric',
                //'sq_feet_max' => 'required_with:sq_feet_min|numeric|greater_than_field:sq_feet_min',
                //'bedrooms' => 'required',
                //'bathrooms' => 'required',
                //'until' => 'required',
                'budget' => 'required' // |numeric
            ], $errorMessages);
        }

        $errors = $validateList->errors()->all();

        if( count($errors) == 0 ) {
        	$list->save();
        } else {
            return response( $errors );
        }

        return response('added');
	}

    // update customer profile info
    public function updateProfile(Request $request) 
    {

        try {

            $user = $request->user();
            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);

            if( $request->file('photo') )
            {
                $errorMessages = [];

                $validateImage = Validator::make($request->all(), [
                    'photo' => 'mimes:jpeg,bmp|max:10000'
                ])->validate();
                   
                $photoName = str_slug($request->user()->name) . '-' . Carbon::now()->format('omd') . '.jpg';
                $user->photo = $photoName;
                Storage::disk('world')->putFileAs('profile-photos', $request->file('photo'), $photoName);
            }

            $user->saveOrFail();

            return redirect()->back()->withSuccess('Profile successfuly updated.');

        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();
            return redirect()->back()
                             ->withErrors($errors)
                             ->withInput();

        }

    }

    public function sendMessage(Agent $agent, Request $request)
    {
        /*$markdown = new Markdown(view(), config('mail.markdown'));
        $subject = 'Customer Inquiry of your Proposal';
        $message = 'test';
        $customer = Auth::user();

        return $markdown->render('email.send-message', compact('message','subject', 'customer'));*/

        if( $request->has('message') )
        {
            Mail::to($agent->asUser->email)->send(new SendMessage('Customer Inquiry of your Proposal', $request->message));
            return response('success');
        } else {
            return response('error');
        }
    }


    // CHAT METHODS
    public function setChatID(Request $request) 
    {

        $customer = $request->user()->asCustomer;

        $customer->setOtherData([
            'chat_id' => $request->chatID
        ]);

    }

    public function getAgentChatID(Request $request)
    {

        $agent = Agent::find($request->userID);

        $chatID = $agent->getOtherData('chat_id');

        return response($chatID);

    }

    public function updateList(Request $request, Listing $list)
    {
        $list->sq_feet_min = $request->has('sq_feet_min') ? $request->sq_feet_min : $list->sq_feet_min;
        $list->sq_feet_max = $request->has('sq_feet_max') ? $request->sq_feet_max : $list->sq_feet_max;
        $list->bathrooms = $request->has('bathrooms') ? $request->bathrooms : $list->bathrooms;
        $list->bedrooms = $request->has('bedrooms') ? $request->bedrooms : $list->bedrooms;
        $list->save();

        return view('user.customer.partials.ajax-proposal-row', compact('list'));
    }

}
