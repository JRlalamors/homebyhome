<?php

namespace App\Http\Controllers\User;

use App\User;
use ImageResize;
use Carbon\Carbon;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Proposal;
use App\Model\Activity;
use App\Model\Customer;
use ValidationException;
use Illuminate\Http\Request;
use App\Mail\SubmittedProposalAgent;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AgentController extends Controller
{
    
    public function account(Request $request) 
    {
        $user = $request->user();
        $agent = $user->asAgent;

        $lazyLoad = ['customer' => function($customer) {
            $customer->with(['asUser']);
        }];

    	$deals = $agent->deals()->with($lazyLoad)
            ->where('closed_deal', 0)
            ->whereDoesntHave('selectedAgent')
            ->whereDoesntHave('proposals', function ($p) use ($agent){
                return $p->where('agent_id', '=', $agent->id);
            })->get()->sortByDesc('until');

    	$customers = $agent->customers()->get();

        $mainOps = $agent->outstandingDealsWithTimestamps()->sortByDesc('pivot.created_at');
        $othersOps = $agent->deals()->with($lazyLoad)
            ->whereDoesntHave('selectedAgent')
            ->whereHas('proposals',  function ($p) use ($agent){
                return $p->where('agent_id', '=', $agent->id);
            })->get();
        $OPs = $mainOps->merge($othersOps)->sortByDesc('until');

        $zillowApi = $agent->zillowApi();

    	return view('user.agent.index', compact('deals', 'customers', 'OPs', 'zillowApi'));
    }

    public function setZillowName(Request $request)
    {          
        try {

            $agent = Agent::find($request->user()->asAgent->id);
            $agent->zillow_url = $request->has('zillow_name') ? $request->zillow_name : null;
            $agent->linkedin_url = $request->has('linkedin_url') ? $request->linkedin_url : null;
            $agent->website_url = $request->has('website_url') ? $request->website_url : null;
            $agent->save();
			
            $user = User::find($request->user()->id);       			
			if( $request->has('new_password') && $request->has('confirm_new_password') ) 
            {
                if( $request->confirm_new_password == $request->new_password ) 
                {
                    $user->password = bcrypt($request->new_password);
                } else {
                    $user->password = '';
                }
				
                $user->save();
            }			

            return redirect()->back()->withSuccess('Account settings updated.');

        } catch ( \Watson\Validating\ValidationException $e ) {
            $errors = $e->getErrors();
            return redirect()->back()->withErrors($errors)->withInput();
        }

    }

    public function accountListDetails(Listing $list, Request $request) 
    {

        $listDetail = '<label>Type:</label> ' . $list->type() . '<br>';
        $listDetail .= '<label>Property Type:</label> ' . $list->property->human_display . '<br>';
        $listDetail .= '<label>Address:</label> ' . $list->complete_address . '<br>';
        $listDetail .= '<label>Zip / Postal:</label> ' . $list->zip_postal . '<br>';
        $listDetail .= '<label>Square Feet:</label> ' . $list->square_feet . '<br>';
        $listDetail .= '<label>Bedrooms:</label> ' . $list->bedrooms . '<br>';
        $listDetail .= '<label>Bathrooms:</label> ' . $list->bathrooms . '<br>';
        $listDetail .= '<label>' . ($list->list_type == 'seller' ? 'Selling' : 'Buying') . ' until:</label> ' . ($list->text_until ? $list->text_until : with(new Carbon($list->until))->format('M d, o'));

        
        if( $list->agentHasProposal($request->user()->asAgent->id) )
        {

            $proposal = $list->proposal($request->user()->asAgent->id);

            if( $list->list_type == 'seller' )
            {
                if( $proposal->rebate_type == 'percentage' )
                {
                    $proposalType = 'Total Commission Rate';
                } else {
                    $proposalType = 'Flat Fee';
                }

            } else {
                if( $proposal->rebate_type == 'percentage' )
                {   
                    $proposalType = 'Commission Rebate';
                } else {
                    $proposalType = 'Cash Rebate';                    
                }
            }

            $toReply = '<h4>Proposal</h4>';
            $toReply .= '<label>Proposal type:</label> ' . $proposalType . '<br>';
            $toReply .= '<label>Rebate:</label> ' . $proposal->rebate_format . '<br>';
            $toReply .= '<label>Why should you hire me?: </label> ' . $proposal->getOtherData('propose_1') . '<br>';
            $toReply .= '<label>What do I do better than most agents?: </label> ' . $proposal->getOtherData('propose_2') . '<br>';

        } else {
            if ( $list->list_type == 'seller' ) {
                $toReply = '<div class="default-form">' .
                    '<div class="form-group">' .
                    '<label>Proposal Type:</label>' .
                    '<select class="form-control rebate-type" name="rebate_type">' .
                    '<option value="percentage">Total Commission Rate</option>' .
                    '<option value="fixed">Flat Fee</option>' .
                    '</select>' .
                    '</div>';
            } else {
                $toReply = '<div class="default-form">' .
                    '<div class="form-group">' .
                    '<label>Proposal Type:</label>' .
                    '<select class="form-control rebate-type" name="rebate_type">' .
                    '<option value="percentage">Commission Rebate</option>' .
                    '<option value="fixed">Cash Rebate</option>' .
                    '</select>' .
                    '</div>';
            }

            $toReply .= '<div class="form-group">' .
                        '<label>Rebate:</label>' .
                        '<div class="input-group">' .
                            '<div class="dollar input-group-addon"><i class="fa fa-dollar"></i></div>' .
                            '<input type="number" name="rebate" min="1" max="6" class="form-control rebate">' .
                            '<div class="percentage input-group-addon"><i class="fa fa-percent"></i></div>' .
                        '</div>' .
                    '</div>' .
                    '<div class="form-group">' . 
                        '<label>What do I do better than most agents?</label>' .
                        '<textarea name="propose_1" class="form-control propose-1" disabled>'.$request->user()->asAgent->getOtherData('question_1').'</textarea>' .
                    '</div>' .
                    '<div class="form-group">' .
                        '<label>Why should you hire me?</label>' .
                        '<textarea name="propose_2" class="form-control propose-2" disabled>'.$request->user()->asAgent->getOtherData('question_2').'</textarea>' .
                    '</div>' .
                    '<div class="form-group text-right">' . 
                        '<button class="btn btn-success submit-proposal">Submit</button>' .
                        ' <button class="btn btn-danger" data-dismiss="modal">Cancel</button>' .
                    '</div>' .
                '</div>';
        }

        return response()->json(compact('listDetail', 'toReply'));
    }

    public function accountListCustomerDetails(Listing $list, Request $request)
    {
        $customer = $list->customer;

        return view('user.agent.customer-list', compact('list', 'customer'));
    }

    public function accountSubmitProposal(Listing $list, Request $request)
    {
        try {
            $proposal = new Proposal;

            $proposal->customer_id = $list->customer->id;
            $proposal->agent_id = $request->user()->asAgent->id;
            $proposal->listing_id = $list->id;

            $proposal->add($request);

            // create activity
            $activity = new Activity;
            $action = 'Sent a proposal for ' . $list->customer->asUser->name . '\'s list.';
            $activity->addActivity($request->user()->id, $action);

            if( $request->ajax() ) 
            {
                Mail::to($list->customer->asUser->email)->send(new SubmittedProposalAgent($list, $request->user()->asAgent));

                return response()->json([
                    'success' => 'Proposal sent.',
                    'route' => route('agent.account')
                ]);
            } else {                
                return redirect()->route('agent.list.details', $list)->withSuccess('Proposal sent.');
            }


        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();

            return response()->json(compact('errors'));

        }

    }

    public function profile(Request $request) 
    {

        $agent = $request->user();

        $zillowApi = $agent->asAgent->zillowApi();

        return view('user.agent.profile', compact('agent', 'zillowApi'));
    }

    public function updateProfile(Request $request)
    {

        try {

            $agent = $request->user();
            $agent->first_name = $request->first_name;
            $agent->last_name = $request->last_name;
            $agent->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);

            if( $request->file('photo') )
            {

                $errorMessages = [];

                $validateImage = Validator::make($request->all(), [
                    'photo' => 'mimes:jpeg,bmp|max:10000'
                ])->validate();
                   
                $photoName = str_slug($request->user()->name) . '-' . Carbon::now()->format('omd') . '.jpg';
                $agent->photo = $photoName;
                Storage::disk('world')->putFileAs('profile-photos', $request->file('photo'), $photoName);
            }

            $agent->save();

            return redirect()->back()->withSuccess('Profile successfuly updated.');

        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();

            return redirect()->back()->withErrors($errors);

        }

    }

    public function updateProfileQA(Request $request) 
    {

         try {

            $agent = $request->user()->asAgent;
            
            $agent->setOtherData([
                'question_1' => $request->question_1,
                'question_2' => $request->question_2,
            ]);

            return redirect()->back()->withSuccess('Q+A successfuly updated.');

        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();

            return redirect()->back()->withErrors($errors);

        }

    }

    public function customerProfile(Customer $customer) 
    {
        $customer = $customer->load('asUser');

        $listings = $customer->listings->each(function($list) {
            $list->load('property');
        })->sortByDesc('created_at');

        return view('user.agent.customer-profile', compact('customer', 'listings'));
    }

    // set other data
    public function saveSettings(Request $request) 
    {

    	try {
	    	$request->user()->asAgent->setOtherData([
	    		'question_1' => $request->question_1,
	    		'question_2' => $request->question_2
	    	]);

	    	return redirect()->back()->withSuccess('Q+A successfuly updated.');

    	} catch ( \Watson\Validating\ValidationException $e ) {
    		$errors = $e->getErrors();

    		return redirect()->back()->withErrors($errors)->withInput();
    	}

    }

    public function listDetails(Listing $list)
    {
        $list = $list->load('property', 'customer.asUser');
        $resizedPhoto = null;
        $agent = Auth::user()->asAgent;

        if ($agent) {
            if( $agent->listHasProposal( $list->id ) )
            {
                $proposal = Proposal::where('listing_id', $list->id)->where('agent_id', $agent->id)->first();
            } else {
                $proposal = null;
            }
        } else {
            abort(404);
        }

        // profile photo
        if( $list->customer->asUser->photo )
        {        
            $profilePhoto = asset(Storage::url('profile-photos/' . $list->customer->asUser->photo));
            $resizedPhoto = ImageResize::make($profilePhoto)->fit(278, 278)->encode('data-url');
        }

        return view('user.agent.list.details', compact('list', 'proposal', 'resizedPhoto', 'agent'));
    }

    public function addProposal(Listing $list, Request $request) 
    {

        $agent = $request->user()->asAgent;

        return view('user.agent.list.add', compact('list', 'agent'));
    }

    public function sendProposal(Listing $list, Request $request)
    {
        try {

            $proposal = new Proposal;

            $proposal->customer_id = $list->customer->id;
            $proposal->agent_id = $request->user()->asAgent->id;
            $proposal->listing_id = $list->id;           

            $proposal->add($request);

            $proposal->setOtherData([
                'propose_1' => $request->propose_1,
                'propose_2' => $request->propose_2
            ]);

            // create activity
            $activity = new Activity;
            $action = 'Sent a proposal for ' . $list->customer->asUser->name . '\'s list.';
            $activity->addActivity($request->user()->id, $action);

            // send mail
            Mail::to($list->customer->asUser->email)->send(new SubmittedProposalAgent($list, $request->user()->asAgent));

            return redirect()->route('agent.list.details', $list)->withSuccess('Proposal sent.');

        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();

            return redirect()->back()->withErrors($errors)->withInput();

        }

    }

    public function editProposal(Listing $list, Proposal $proposal, Request $request) 
    {

        $agent = $request->user()->asAgent;

        return view('user.agent.list.edit', compact('list', 'proposal', 'agent'));
    }

    public function updateProposal(Listing $list, Proposal $proposal, Request $request)
    {

        try {

            $proposal->customer_id = $list->customer->id;
            $proposal->agent_id = $request->user()->asAgent->id;
            $proposal->listing_id = $list->id;

            $proposal->add($request);

            $proposal->setOtherData([
                'propose_1' => $request->propose_1,
                'propose_2' => $request->propose_2
            ]);

            return redirect()->route('agent.list.details', $list)->withSuccess('Proposal updated.');

        } catch( \Watson\Validating\ValidationException $e ) {

            $errors = $e->getErrors();

            return redirect()->back()->withErrors($errors)->withInput();

        }        

    }


    // CHAT METHODS
    public function setChatID(Request $request) 
    {

        $agent = $request->user()->asAgent;

        $agent->setOtherData([
            'chat_id' => $request->chatID
        ]);

    }

}
