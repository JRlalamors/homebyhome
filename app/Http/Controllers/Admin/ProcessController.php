<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Carbon\Carbon;
use App\Model\Role;
use App\Model\Listing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProcessController extends Controller
{
    
	public function index() 
	{
		return view('admin.process.index');
	}

	public function deals(Request $request) 
	{

		$currentDate = Carbon::now();

		$listings = new Listing;
		$pageTitle = 'Current Deals Out For Proposal (No Agent Selected)';

		if( $request->dwa ) 
		{	

			$listings = $listings->whereDoesntHave('agents')->paginate(10);

			// Deals without agent over 10 days
			if( $request->dwa == 10 ) {
				$pageTitle = 'Deals without an agent over 10 days';
				$listings = $listings->filter(function($listing) use($currentDate) {
								$listingDate = with( new Carbon($listing->created_at));
								return $listingDate->diffInDays($currentDate) > 10 && $listing->closed_deal == 0;
							})->load('customer.asUser');
			} else {
				// Deals without agent over 30 days
				$pageTitle = 'Deals without an agent over 30 days';
				$listings = $listings->filter(function($listing) use($currentDate) {
								$listingDate = with( new Carbon($listing->created_at));
								return $listingDate->diffInDays($currentDate) > 30 && $listing->closed_deal == 0;
							})->load('customer.asUser');
			} 

		} elseif( $request->dnc ) {			
			$pageTitle = 'Deals with an agent that have\'nt closed yet';
			$listings = $listings->whereHas('agents')
								 ->where('closed_deal', 0)
								 ->paginate(10);

		} else {
			$listings = $listings->with(['customer.asUser'])->paginate(10);
		}

		return view('admin.process.deals', compact('listings', 'pageTitle'));
	}


}
