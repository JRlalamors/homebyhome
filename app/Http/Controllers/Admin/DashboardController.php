<?php

namespace App\Http\Controllers\Admin;

use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    
	public function index() {

        $customers = new Customer;
        $customers = $customers->search(['asUser', 'listings'])->sortByDesc('id');


		$agents = new Agent;		
		$agents = $agents->search(['asUser'])->sortByDesc('id');
		
		$deals = Listing::with('customer')->where('agent_id', '!=', null)->get();

		return view('admin.index', compact('customers', 'agents', 'deals', 'title', 'description'));
	}

}
