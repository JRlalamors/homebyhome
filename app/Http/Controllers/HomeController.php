<?php

namespace App\Http\Controllers;

use App\Model\Activity;
use App\User;
use Carbon\Carbon;
use App\Model\Agent;
use App\Model\Listing;
use App\Model\Customer;
use App\Mail\RequestCall;
use App\Mail\RequestContact;
use Illuminate\Http\Request;
use App\Mail\CustomerSignup;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function userLogin() 
    { 

        if( Auth::user() ) {
            return redirect('/');
        }
        else {
            return view('auth.login');   
        }
    }

    public function adminLogin() 
    {
        $admin = true;
        return view('auth.admin.login');
    }

    public function home() 
    {

        $title = 'Find Top Real Estate Agents Near Me at Home By Home';
        $description = 'Home By Home helps home buyers and home sellers find the right real estate agent at the right price.';

        return view('index', compact('title', 'description'));
    }

    public function about() 
    {
        return view('about');
    }

    public function terms() 
    {
        return view('terms');
    }

    public function privacy() 
    {
        return view('privacy');
    }

    public function resources() 
    {
        return view('resources');
    }
    
    public function contact() 
    {
        return view('contact');
    }

    public function findAgent() 
    {       
        $agents = Agent::all();

        return view('find-agent', compact('agents'));
    }    

    /*
    * Registration
    **/
    public function register() {

        $title = 'Find A Real Estate Agent Near Me | Home By Home';
        $description = 'Find a top real estate agent or realtor near me at HomeByHome.com';

        return view('register', compact('title', 'description'));
    }

    public function getForms(Request $request)
    {
        if ($request->type == 'seller') {
            return view('partials.form-seller');
        } else {
            return view('partials.form-buyer');
        }
    }

    public function validateForm(Request $request)
    {   
        $user = new User;
        $customer = new Customer;
        $list = new Listing;

        // validating user
        if( array_key_exists('first_name', $request->all()) || $request->has('first_name') || array_key_exists('last_name', $request->all()) || $request->has('last_name') || array_key_exists('email', $request->all()) || $request->has('email') || array_key_exists('password', $request->all()) || $request->has('password') )
        {
            $user->fill($request->all());
            $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
            $user->password = $request->password ? bcrypt($request->password) : null;
            $user->isValid();
        }

        // validating list               
        $list->fill($request->all());
        //$list->until = with(new Carbon($request->until))->format('o-m-d');
        $list->closed_deal = 0;

        $errorMessages = [
            'sq_feet_max.greater_than_field' => 'The square feet max field must be higher than square feet min field.'
        ];

        $reqArray = [];
        $validateArr = [];

        if( array_key_exists('zip_postal', $request->all()) || $request->has('zip_postal') ) {
            $reqArray['zip_postal'] = $request->zip_postal;
            $errorMessages['zip_postal.required'] = 'Location is required.';
            $validateArr['zip_postal'] = 'required|numeric|min:5';
        }

        if( array_key_exists('budget', $request->all()) || $request->has('budget') ) {
            $reqArray['budget'] = preg_replace(array('/\,/', '/\$/'), array(''), $request->budget);
            $validateArr['budget'] = 'required'; // |numeric
            $reqArray['text_budget'] = $request->budget; //preg_replace(array('/\,/', '/\$/'), array(''), )
            $validateArr['text_budget'] = 'required'; // |numeric
        }

        /*if( (array_key_exists('sq_feet_min', $request->all()) || $request->has('sq_feet_min')) && (array_key_exists('sq_feet_max', $request->all()) || $request->has('sq_feet_max')) ) {
            $reqArray['sq_feet_min'] = $request->sq_feet_min;
            $reqArray['sq_feet_max'] = $request->sq_feet_max;

            if( $request->list_type == 'seller' )            
            {
                $validateArr['sq_feet_max'] = 'required|numeric';
            } else {            
                $validateArr['sq_feet_min'] = 'required_with:sq_feet_max|numeric';
                $validateArr['sq_feet_max'] = 'required_with:sq_feet_min|numeric|greater_than_field:sq_feet_min';
            }
        }*/

        /*if( array_key_exists('bedrooms', $request->all()) || $request->has('bedrooms') ) {
            $reqArray['bedrooms'] = $request->bedrooms;
            $validateArr['bedrooms'] = 'required';
        }

        if( array_key_exists('bathrooms', $request->all()) || $request->has('bathrooms') ) {
            $reqArray['bathrooms'] = $request->bathrooms;
            $validateArr['bathrooms'] = 'required';
        }*/

        if( array_key_exists('until', $request->all()) || $request->has('until') ) {
            /*$reqArray['until'] = $request->until;
            $validateArr['until'] = 'required'; //|date*/
            $reqArray['text_until'] = $request->until;
            $validateArr['text_until'] = 'required'; //|date
        }

        $validateList = Validator::make($reqArray, $validateArr, $errorMessages);

        $validateList->fails();



        $errors = $user->getErrors()->merge($customer->getErrors());         
        if( $validateList->errors()->count() > 0 )
        {
            $errors = $validateList->errors()->merge($errors);
        }

        if( count( $errors ) > 0 ) {
            return response( $errors );
        }

        return response('validated');
    }

    public function storeRegistration(Request $request)
    {
        // create models
        $user = new User;
        $customer = new Customer;
        $list = new Listing;

        // create new user
        $user->fill($request->all());
        $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
        $user->password = bcrypt( $request->password );
        $user->role_id = 0;
        
        // register as a customer                
        $customer->fill($request->all());
        $customer->status_to_lender = $request->has('status_to_lender') ? $request->status_to_lender : 'not-yet';

        // Add List
        $list->fill($request->all());

        //$list->until = $until;
        $list->text_until = $request->until;
        $list->budget = (int) preg_replace(array('/\,/', '/\$/'), array(''), $request->numeric_budget);
        $list->text_budget = $request->list_type == 'seller' ? $request->budget : '$' . number_format($request->budget, 0);
        $list->closed_deal = 0;

        $errorMessages = [
            'sq_feet_max.greater_than_field' => 'The square feet max field must be higher than square feet min field.'
        ];
        
        if( $request->list_type == 'seller' ) {
            $list->city = $request->city;
            $list->state = $request->state;
            $list->other_data = json_encode(array('street_address' => $request->street_address, 'unit_no' => $request->unit_no));            

            // validating rules
            $validateList = Validator::make($request->all(), [
                'list_type' => 'required',
                'property_type_id' => 'required',
                //city' => 'required',
                //'state' => 'required',
                'zip_postal' => 'required|numeric|min:5',
                //'sq_feet_max' => 'required:numeric',
                //'bedrooms' => 'required',
                //'bathrooms' => 'required',
                //'until' => 'required', // |date
                'budget' => 'required' // |numeric
            ], $errorMessages);
        }
        else {
            // validating rules
            $validateList = Validator::make($request->all(), [
                'list_type' => 'required',
                'property_type_id' => 'required',
                'zip_postal' => 'required|numeric|min:5',
                //'sq_feet_min' => 'required_with:sq_feet_max|numeric',
                //'sq_feet_max' => 'required_with:sq_feet_min|numeric|greater_than_field:sq_feet_min',
                //'bedrooms' => 'required',
                //'bathrooms' => 'required',
                //'until' => 'required', //|date
                'budget' => 'required' //|numeric
            ], $errorMessages);
        }
        
        $user->isValid();
        $customer->isValid();
        $validateList->fails();

        $errors = $user->getErrors()->merge($validateList->errors()->all());
        $errors = $customer->getErrors()->merge($errors);

        // Check models        
        if( count($errors) == 0 ) {
            
            $user->save();
            $customer->user_id = $user->id;
            $customer->save();
            $list->customer_id = $customer->id;
            $list->save();

            $activity = new Activity;
            $action = $user->name . ' registered';
            $activity->addActivity($customer->asUser->id, $action);

            $message = 'added';
        } else {
            $message = 'error';

            return response()->json(compact('message'));
        }

        return response()->json(compact('message', 'user'));
    }

    public function success(User $user = null) {


        if( $user )
        {
            Auth::login($user, true);

            if( $user->isAgent() )
            {
                return redirect()->route('agent.account');
            } else {
                return redirect()->route('customer.account');
            }

        }

        return view('register-success');
    }

    public function fbRegister(User $user) 
    {
        $customer = new Customer;
        $customer->user_id = $user->id;
        $customer->status_to_lender = 'not-yet';    

        if( $customer->save() ) 
        {
            Auth::login($user, true);

            return redirect()->route('customer.account');
        }
    }    

    // email
    public function requestCall(Request $request)
    {
        $errorMessages = [];

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required|numeric'
        ], $errorMessages);

        if( count( $validate->errors() ) == 0 )
        {
            Mail::to([config('homebyhome.contact_email'), config('homebyhome.contact_email_2')])->cc(config('app.contact_sytian'))->send(new RequestCall($request->name, $request->email, $request->phone));
            return redirect()->back()
                        ->withSuccess('Your request has been received.');
        } else {
            $errors = $validate->errors();
            return redirect()->back()->withErrors($errors);
        }


    }

    public function contactUs(Request $request)
    {          

        $errorMessages = [];

        $request->phone = (int) preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone);

        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'phone' => 'required'
        ], $errorMessages);

        if( count( $validate->errors() ) == 0 )
        {

            Mail::to(config('app.contact_email'))->cc(config('app.contact_sytian'))->send(new RequestContact($request->first_name, $request->last_name, $request->email, $request->company, $request->phone, $request->question_comment));

            return redirect()->back()->withSuccess('Your request has been sent.');

        } else {

            $errors = $validate->errors();

            return redirect()->back()->withErrors($errors);

        }        
        
    }

}
