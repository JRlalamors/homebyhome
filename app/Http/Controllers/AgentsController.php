<?php

namespace App\Http\Controllers;

use App\Model\Activity;
use App\User;
use App\Model\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AgentsController extends Controller
{
    public function index() 
    {

        $title = 'Find Home Buyers and Home Sellers Near Me | Home By Home';
        $description = 'Realtors and real estate agent win more business and sell more homes using HomeByHome.com';

    	return view('agent.index', compact('title', 'description'));
    }

    public function register()
    {

        $title = 'Register at Home By Home To Find Home Buyers and Home Sellers Near You';
        $description = 'Win more business and sell more homes using HomeByHome.com';

    	return view('agent.register', compact('title', 'description'));
    }

    public function validateForm(Request $request) 
    {

        $user = new User;
        $agent = new Agent;

        $user->fill($request->all());
        $user->password = bcrypt("");
        $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
        $user->isValid();

        $errorMessages = [];
        $reqArray = [];
        $validateArr = [];

        if( array_key_exists('current_role', $request->all()) || $request->has('current_role') ) {
            $reqArray['current_role'] = $request->current_role;
            $validateArr['current_role'] = 'required';
        }

        if( array_key_exists('work_with', $request->all()) || $request->has('work_with') ) {
            $reqArray['work_with'] = $request->work_with;
            $validateArr['work_with'] = 'required';
        }


        $validateList = Validator::make($reqArray, $validateArr, $errorMessages);
        $validateList->fails();

        $errors = $user->getErrors()->merge($agent->getErrors());

        if( count( $errors ) > 0 ) {
            return response( $errors );
        }

        echo 'validated';

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function submitRegistration(Request $request)
    {
        $password = str_random(8);

        $user = new User;   
        $user->fill($request->all());
        $user->phone_num = preg_replace(array('/\+/', '/\(/', '/\)/', '/\s/', '/\./', '/\-/', '/[a-zA-Z]/'), array(''), $request->phone_num);
        $user->role_id = 0;
        $user->password = bcrypt($password);

        // set details for agent
        $agent = new Agent;
        $agent->fill($request->all());
        // $request->approved = 0;
        // $request->rejected = 0;

        $user->isValid();
        $agent->isValid();

        $errors = $user->getErrors()->merge($agent->getErrors());   

        if( count( $errors ) == 0 ) 
        {
            $user->save();
            $agent->user_id = $user->id;
            $agent->save();

            $activity = new Activity();
            $action = $user->name . ' registered';
            $activity->addActivity($agent->asUser->id, $action);

            $agent->setOtherData(['plain_password' => $password, 'facebook_url' => $request->facebook_url, 'yelp_url' => $request->yelp_url]);

        } else {
            return response( $errors );
        }
        
        echo 'added';
        // return response()->json(['added' => true]);
    }

    public function fbRegister(User $user) 
    {
        $agent = new Agent;
        $agent->user_id = $user->id;
        $agent->current_role = 'individual-agent';
        $agent->work_with = 'both';

        if( $agent->save() ) 
        {
            Auth::login($user, true);

            return redirect()->route('agent.account');
        }

    }

    public function success() 
    {
        return view('agent.register-success');
    }
}
